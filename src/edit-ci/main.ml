open! Base
open! Merbocop_lib

let dbgf f =
  Fmt.kstr (Caml.Format.eprintf "@[<2>[merbocop-edit-ci-debug]@ %s@]@.%!") f

module Path = struct
  include Caml.Filename

  let ( // ) = concat
end

module Job = struct
  type t =
    { name: string
    ; docker_image: string
    ; artifacts: string list
    ; dependencies: string list
    ; script: string list
    ; only_schedule: bool }

  let make ?(only_schedule = false) ?(dependencies = []) ~docker_image ~script
      ~artifacts name =
    {name; docker_image; artifacts; only_schedule; script; dependencies}
end

module Rendered = struct
  type file = {path: string; content: string list}

  let file path content = {path; content}

  type branch =
    { name: string
    ; files: file list
    ; cron: string option
    ; required_variables: string list }

  type t = {branches: branch list}

  let branch_names {branches} = List.map branches ~f:(fun b -> b.name)

  let pp_hum ppf {branches} =
    let open Fmt in
    pf ppf "@[<v># Pipeline@." ;
    List.iter branches ~f:(fun {name; files; cron; required_variables} ->
        pf ppf "## Branch `%s` (cron: %S, variables: %a)@." name
          (Option.value ~default:"N/A" cron)
          Fmt.Dump.(list string)
          required_variables ;
        List.iter files ~f:(fun {path; content} ->
            pf ppf "File `%s`:@." path ;
            pf ppf "@.```@.%a@.```@.@." (vbox (list ~sep:cut string)) content ) ) ;
    pf ppf "@]"
end

module Pipeline = struct
  type t =
    { branch: string
    ; description: string list
    ; jobs: Job.t list
    ; schedule: [`Every_n_hours of int]
    ; required_variables: string list }

  let make ?(description = []) ?(schedule = `Every_n_hours 2)
      ?(required_variables = []) ~branch jobs =
    {branch; description; jobs; schedule; required_variables}

  let expected_cron pipeline =
    match pipeline.schedule with
    | `Every_n_hours 1 -> "0 * * * *"
    | `Every_n_hours n -> Fmt.str "0 */%d * * *" n
end

let render_pipelines pipelines =
  let open Rendered in
  let open Pipeline in
  let branches =
    List.map pipelines ~f:(fun pipeline ->
        let files =
          [ file "README.md"
              ( [ "# Merbocop CI Branch (DO NOT EDIT)"
                ; "This branch is managed by `src/edit-ci/main.ml`."; "" ]
              @ pipeline.description )
          ; file ".gitlab-ci.yml"
              ( ["# See README.md"; "stages:"; "  - run"]
              @ List.concat_map pipeline.jobs
                  ~f:
                    Job.(
                      let indent = List.map ~f:(Fmt.str "  %s") in
                      let section name l = name :: indent l in
                      let indented_dashes l =
                        indent (List.map l ~f:(Fmt.str "- %s")) in
                      fun job ->
                        Fmt.str "%s:" job.name
                        ::
                        indent
                          ( [ "stage: run"; Fmt.str "image: %s" job.docker_image
                            ; "artifacts:" ]
                          @ indent ("paths:" :: indented_dashes job.artifacts)
                          @ ( match job.dependencies with
                            | [] -> []
                            | deps ->
                                ["dependencies:"] @ indented_dashes deps
                                @ ["needs:"] @ indented_dashes deps )
                          @ ( if job.only_schedule then
                              section "only:"
                                (section "variables:"
                                   ["- $CI_PIPELINE_SOURCE == \"schedule\""] )
                            else [] )
                          @ section "script:"
                              (List.map job.script ~f:(Fmt.str "- %s")) )) ) ]
        in
        { name= pipeline.branch
        ; files
        ; cron= Some (expected_cron pipeline)
        ; required_variables= pipeline.required_variables } ) in
  {branches}

module Default_remote = struct
  let name () =
    match Caml.Sys.getenv "edit_ci_remote" with
    | s -> s
    | exception _ -> "test-remote"

  let ensure () = Fmt.kstr System.command_unit_or_fail "git fetch %s" (name ())
end

module Testable = struct
  let on = ref false

  let () =
    match Caml.Sys.getenv "testing_edit_ci" with
    | "true" -> on := true
    | _ | (exception _) -> on := false

  let remote_name () = Default_remote.name ()
  let git_remote_ref s = Fmt.str "refs/remotes/%s/%s" (remote_name ()) s
  let git_branch_with_remote s = Fmt.str "%s/%s" (remote_name ()) s

  let extra_pipelines () =
    if !on then
      let script =
        [ {|echo CI_PIPELINE_SOURCE $CI_PIPELINE_SOURCE|}; "mkdir artifact0"
        ; "echo world > artifact0/hello" ] in
      [ Pipeline.make ~branch:"fake-one" ~schedule:(`Every_n_hours 1)
          ~required_variables:["a_variable"; "another_variable"]
          ~description:["This is a fake pipeline."]
          [ Job.make "job-cronly" ~docker_image:"alpine" ~only_schedule:true
              ~artifacts:["artifact0"] ~script
          ; Job.make "job1" ~docker_image:"ubuntu" ~artifacts:["artifact0"]
              ~script ] ]
    else []
end

let current_config : Pipeline.t list =
  let image ~commit =
    Fmt.str "registry.gitlab.com/oxheadalpha/merbocop:%s-run" commit in
  let test_image = image ~commit:"ac0f37f4" (* bot-actions refactor *) in
  let production_image =
    image ~commit:"bfbf6e8a"
    (*  --post-warnings option *) in
  let production_projects = [("tezos-tezos", "3836952")] in
  let merbocop_project = ("oxheadalpha-merbocop", "14777851") in
  let test_projects = [("smondet-tezos", "9385204"); merbocop_project] in
  let all_projects = production_projects @ test_projects in
  let smbot_user = "4777401" in
  let tzbot_user = "4984456" in
  let cron image name script =
    Job.make ~only_schedule:true name ~docker_image:image ~artifacts:[name]
      ~script in
  let test_cron name = cron test_image name in
  let production_cron name = cron production_image name in
  let script ?(allow_failures = false) name todo =
    let artifact_path = name in
    [ Fmt.str "mkdir -p %s" artifact_path
    ; Fmt.str
        "merbocop full-inspection --dump-cache=%s/cache-dump/ \
         --write-body=%s/body.md %s %s"
        artifact_path artifact_path
        (if allow_failures then "--allow-failures" else "")
        (let whole project n = Fmt.str " --whole=%s:%d" project n in
         let user id token = Fmt.str " --self-id=%s --access=%s" id token in
         match todo with
         | `Report project -> whole project 200
         | `Comment (project, user_id, user_token, kind_of_run) ->
             whole project 80 ^ user user_id user_token ^ " --post-on-self"
             ^ Fmt.str " --post-warnings-to %s:%d:%s" (snd merbocop_project) 51
                 ( match kind_of_run with
                 | `Test -> "non-empty"
                 | `Production -> "non-empty" )
         | `Label (project, user_id, user_token) ->
             whole project 80 ^ user user_id user_token
             ^ (* " --set-doc-label=doc-only --set-approved-label=approved" *)
             " --set-doc-label=doc-only"
         | `Approve (project, user_id, user_token) ->
             whole project 80 ^ user user_id user_token ^ " --approve-doc-only"
        ) ] in
  let report (project_name, project_id) =
    let name = Fmt.str "report-%s" project_name in
    test_cron name (script name (`Report project_id)) in
  let cron_and_user =
    let f cron n which_script = cron n (script n which_script) in
    function
    | `Test -> (f test_cron, smbot_user, "$smbot_token")
    | `Production -> (f production_cron, tzbot_user, "$tezbocop_token") in
  let do_commenting which (project_name, project_id) =
    let f, user_id, user_token = cron_and_user which in
    f
      (Fmt.str "comment-%s" project_name)
      (`Comment (project_id, user_id, user_token, which)) in
  let do_labeling which (project_name, project_id) =
    let f, user_id, user_token = cron_and_user which in
    f
      (Fmt.str "label-%s" project_name)
      (`Label (project_id, user_id, user_token)) in
  let do_approving which (project_name, project_id) =
    let f, user_id, user_token = cron_and_user which in
    f
      (Fmt.str "approve-%s" project_name)
      (`Approve (project_id, user_id, user_token)) in
  let markdown_itemize_projects projects =
    List.map projects ~f:(fun (n, i) -> Fmt.str "* `%s` (id: `%s`)." n i) in
  let build_markdown_bodies jobs =
    let open Job in
    let output = "bodies" in
    let builds =
      List.concat_map jobs ~f:(fun job ->
          List.map job.artifacts ~f:(fun p ->
              Fmt.str "pandoc %s/body.md -s --metadata title=%s -o %s/%s.html" p
                p output p ) ) in
    jobs
    @ [ make "build-html-bodies" ~only_schedule:true ~artifacts:[output]
          ~docker_image:"ubuntu"
          ~dependencies:(List.map jobs ~f:(fun j -> j.name))
          ~script:
            ( [ "apt update"; "apt install --yes pandoc"
              ; Fmt.str "mkdir -p %s" output ]
            @ builds ) ] in
  [ Pipeline.make ~branch:"ci-cron-reports" ~schedule:(`Every_n_hours 3)
      ~description:
        ("This pipeline only runs “full” reports on various repositories:"
         :: "" :: markdown_itemize_projects all_projects )
      (List.map all_projects ~f:report |> build_markdown_bodies)
  ; Pipeline.make ~branch:"ci-cron-test-comments" ~schedule:(`Every_n_hours 3)
      ~description:
        ( [ "This cron, runs merbocop in commenting mode, with the testing"
          ; Fmt.str "docker-image (`%s`)" test_image; "on the testing projects:"
          ; "" ]
        @ markdown_itemize_projects test_projects )
      ~required_variables:["smbot_token"]
      (List.map test_projects ~f:(do_commenting `Test) |> build_markdown_bodies)
  ; Pipeline.make ~branch:"ci-cron-test-labeling" ~schedule:(`Every_n_hours 3)
      ~description:
        ( [ "This cron, runs merbocop in `--set-*-label` mode, with the testing"
          ; Fmt.str "docker-image (`%s`)" test_image; "on the testing projects:"
          ; "" ]
        @ markdown_itemize_projects test_projects )
      ~required_variables:["smbot_token"]
      (List.map test_projects ~f:(do_labeling `Test) |> build_markdown_bodies)
  ; Pipeline.make ~branch:"ci-cron-test-approving" ~schedule:(`Every_n_hours 1)
      ~description:
        ( [ "This cron, runs merbocop in `--approve-doc-only` mode,"
          ; "with the testing"; Fmt.str "docker-image (`%s`)" test_image
          ; "on the testing projects:"; "" ]
        @ markdown_itemize_projects test_projects )
      ~required_variables:["smbot_token"]
      (List.map test_projects ~f:(do_approving `Test) |> build_markdown_bodies)
  ; Pipeline.make ~branch:"ci-cron-prod-comments" ~schedule:(`Every_n_hours 6)
      ~description:
        ( [ "This cron, runs merbocop in commenting mode, with the production"
          ; Fmt.str "docker-image (`%s`)" production_image; "on:"; "" ]
        @ markdown_itemize_projects production_projects )
      ~required_variables:["tezbocop_token"]
      ( List.map production_projects ~f:(do_commenting `Production)
      |> build_markdown_bodies )
  ; Pipeline.make ~branch:"ci-cron-prod-labeling" ~schedule:(`Every_n_hours 2)
      ~description:
        ( [ "This cron, runs merbocop in `--set-*-label` mode, with the \
             production"; Fmt.str "docker-image (`%s`)" production_image; "on:"
          ; "" ]
        @ markdown_itemize_projects production_projects )
      ~required_variables:["tezbocop_token"]
      ( List.map production_projects ~f:(do_labeling `Production)
      |> build_markdown_bodies )
  ; Pipeline.make ~branch:"ci-cron-prod-approving" ~schedule:(`Every_n_hours 1)
      ~description:
        ( [ "This cron, runs merbocop in `--approve-doc-only` mode,"
          ; "with the production"
          ; Fmt.str "docker-image (`%s`)" production_image; "on:"; "" ]
        @ markdown_itemize_projects production_projects )
      ~required_variables:["tezbocop_token"]
      ( List.map production_projects ~f:(do_approving `Production)
      |> build_markdown_bodies ) ]
  @ Testable.extra_pipelines ()

let git_assert_remote_of_branch_exists branch =
  let _commit_hash =
    Fmt.kstr System.command_to_string_list_or_fail "git show-ref -s %s"
      (Testable.git_remote_ref branch) in
  ()

module Schedule = struct
  type t =
    {id: int; branch: string; cron: string; variables: (string * string) list}

  let all_current () =
    try
      let project_id =
        let remote = Default_remote.name () in
        let uri =
          Fmt.kstr System.command_to_string_list_or_fail "git remote get-url %s"
            remote
          |> String.concat ~sep:"" in
        uri
        |> String.chop_prefix_if_exists ~prefix:"git@gitlab.com:"
        |> String.chop_prefix_if_exists ~prefix:"https://gitlab.com/"
        |> String.chop_suffix_if_exists ~suffix:".git"
        |> Uri.pct_encode in
      let open Jq in
      let api_get path =
        Web_api.get_json
          ~private_token:
            ( try Caml.Sys.getenv "edit_ci_token"
              with Caml.Not_found ->
                Fmt.failwith "Missing edit_ci_token env-variable." )
          ~retry_options:Web_api.Retry_options.web_api
          (Fmt.kstr Uri.of_string "https://gitlab.com/api/v4%s" path) in
      let json_all_schedules =
        Fmt.kstr api_get "/projects/%s/pipeline_schedules" project_id in
      let parse_schedule json =
        let dict = get_dict json in
        let id = get_field "id" dict |> get_int in
        let branch = get_field "ref" dict |> get_string in
        let cron = get_field "cron" dict |> get_string in
        let variables =
          let field =
            try get_field "variables" dict
            with _ ->
              Fmt.failwith
                "Cannot get the Schedule variables for %S, are you sure your \
                 token has enough access rights to see the project secrets?"
                branch in
          field
          |> get_list (fun obj ->
                 let dict = get_dict obj in
                 ( get_field "key" dict |> get_string
                 , get_field "value" dict |> get_string ) ) in
        {id; branch; cron; variables} in
      let with_details =
        let get_more_info_and_parse obj =
          let dict = get_dict obj in
          let id = get_field "id" dict |> get_int in
          Fmt.kstr api_get "/projects/%s/pipeline_schedules/%d" project_id id
          |> parse_schedule in
        get_list get_more_info_and_parse json_all_schedules in
      with_details
    with e ->
      Fmt.failwith "@[<2>Failed to get current schedules:@ %a@]" Exn.pp e
end

let get_current model =
  let open Rendered in
  let get_file ~branch relative_path =
    try
      let content =
        Fmt.kstr System.command_to_string_list_or_fail "git show %s"
          (Fmt.kstr Path.quote "%s:%s"
             (Testable.git_branch_with_remote branch)
             relative_path ) in
      Some {path= relative_path; content}
    with _ -> None in
  let branches =
    List.filter_map model.branches ~f:(fun branch ->
        try
          git_assert_remote_of_branch_exists branch.name ;
          let files =
            List.filter_map branch.files ~f:(fun f ->
                get_file ~branch:branch.name f.path ) in
          Some {name= branch.name; files; cron= None; required_variables= []}
        with _ -> None ) in
  {branches}

let show_command () =
  let open Cmdliner in
  let open Term in
  let term =
    const (fun () ->
        let rendered = render_pipelines current_config in
        Fmt.pr "%a%!" Rendered.pp_hum rendered ;
        dbgf "Done." )
    $ const () in
  (term, info "show" ~doc:"Show what it would ensure.")

let diff_command () =
  let open Cmdliner in
  let open Term in
  let term =
    const (fun () ->
        Default_remote.ensure () ;
        let model = render_pipelines current_config in
        let current = get_current model in
        let open Rendered in
        let all_extra_branches =
          let cmd = {|git branch -a --format="%(refname:short)"|} in
          System.command_to_string_list_or_fail cmd
          |> List.filter ~f:(fun line ->
                 String.is_prefix line
                   ~prefix:(Testable.git_branch_with_remote "ci-cron")
                 && not
                      (List.exists model.branches ~f:(fun br ->
                           String.equal
                             (Testable.git_branch_with_remote br.name)
                             line ) ) ) in
        let all_schedules = Schedule.all_current () in
        let ppf = Fmt.stdout in
        let outf fmt = Fmt.pf ppf fmt in
        outf "@[" ;
        List.iter model.branches ~f:(fun model_branch ->
            outf "@[<v>" ;
            match
              List.find current.branches ~f:(fun b ->
                  String.equal b.name model_branch.name )
            with
            | None ->
                outf "@,- Branch %s does not exist" model_branch.name ;
                outf "@]%!"
            | Some {files; _} ->
                outf "@,@[<v 4>- Branch %s is there" model_branch.name ;
                List.iter model_branch.files ~f:(fun {path; content} ->
                    outf "@,- File %S" path ;
                    match
                      List.find files ~f:(fun f -> String.equal f.path path)
                    with
                    | Some {content= oc; _}
                      when List.equal String.equal oc content ->
                        outf " is ready."
                    | None -> outf " is absent."
                    | Some _ -> outf " is different." ) ;
                let expected_cron =
                  model_branch.cron |> Option.value ~default:"MISSING" in
                begin
                  match
                    List.find all_schedules ~f:(function
                      | Schedule.{branch; _}
                        when String.equal branch model_branch.name ->
                          true
                      | _ -> false )
                  with
                  | Some Schedule.{cron; variables; _} ->
                      outf "@,- @[Existing schedule uses %a.@]" Fmt.text
                        ( if String.equal cron expected_cron then
                          Fmt.str "the right cron: %S" cron
                        else
                          Fmt.str "the WRONG cron: %S (expected: %S)." cron
                            expected_cron ) ;
                      outf "@,- @[Existing schedule defines %a@]." Fmt.text
                        ( if
                          List.for_all model_branch.required_variables
                            ~f:(fun v ->
                              List.exists variables ~f:(fun (n, _) ->
                                  String.equal v n ) )
                        then
                          Fmt.str "all the required variables (%a ⊂ %a)"
                            Fmt.Dump.(list string)
                            model_branch.required_variables
                            Fmt.Dump.(list string)
                            (List.map ~f:fst variables)
                        else
                          Fmt.str "NOT ENOUGH VARIABLES: %a ⊄ %a"
                            Fmt.Dump.(list string)
                            model_branch.required_variables
                            Fmt.Dump.(list string)
                            (List.map ~f:fst variables) )
                  | None ->
                      outf
                        "@,\
                         - @[There is no schedule, need to add one with@ cron \
                         %S and@ variables: %a@]"
                        expected_cron
                        Fmt.Dump.(list string)
                        model_branch.required_variables
                end ;
                outf "@]%!" ) ;
        List.iter all_extra_branches ~f:(fun name ->
            outf "@,@[<2>- %a@]" Fmt.text
              (Fmt.str
                 "Branch %S has a relevant prefix but is not currently handled."
                 name ) ) ;
        List.iter all_schedules ~f:(fun Schedule.{branch; cron; variables; _} ->
            if
              List.exists model.branches ~f:(fun b ->
                  String.equal b.name branch )
            then ()
            else
              outf
                "@,\
                 @[<2>- There is an orphan Schedule on@ branch %S@ (cron: %S,@ \
                 %s).@]"
                branch cron
                ( match variables with
                | [] -> "no variables"
                | more ->
                    Fmt.str "variables: %a"
                      Fmt.Dump.(list string)
                      (List.map more ~f:fst) ) ) ;
        outf "@,@]%!" ;
        dbgf "Done." )
    $ const () in
  (term, info "diff" ~doc:"Compare the existing with what would be created.")

let ensure_command () =
  let open Cmdliner in
  let open Term in
  let term =
    const (fun push_flag () ->
        Default_remote.ensure () ;
        let rendered = render_pipelines current_config in
        let worktree_path branch = Path.("_worktree" // branch) in
        let _log = ref [] in
        let logf depth fmt =
          Fmt.kstr (fun s -> _log := (depth, s) :: !_log) fmt in
        let open Rendered in
        Exn.protect
          ~f:(fun () ->
            List.iter rendered.branches ~f:(fun branch ->
                let local = Fmt.str "local-%s" branch.name in
                logf 0 "Branch %s" branch.name ;
                logf 1 "local: %s" local ;
                begin
                  try
                    git_assert_remote_of_branch_exists branch.name ;
                    Fmt.kstr System.command_unit_or_fail
                      "git branch -f %s -t %s" (Path.quote local)
                      (Path.quote (Testable.git_branch_with_remote branch.name)) ;
                    logf 2 "-> tracking %s"
                      (Testable.git_branch_with_remote branch.name)
                  with _ ->
                    Fmt.kstr System.command_unit_or_fail "git branch -f %s"
                      (Path.quote local) ;
                    logf 2 "-> freshly created, set upstream to %s"
                      (Testable.git_branch_with_remote branch.name)
                end ;
                let worktree = worktree_path local in
                Fmt.kstr System.command_unit_or_fail
                  "git worktree remove -f %s || :" (Path.quote worktree) ;
                Fmt.kstr System.command_unit_or_fail "git worktree add %s %s"
                  (Path.quote worktree) local ;
                logf 1 "Worktree created: %s" worktree ;
                List.iter branch.files ~f:(fun {path; content} ->
                    let full_path = Path.(worktree // path) in
                    System.write_lines full_path content ;
                    Fmt.kstr System.command_unit_or_fail "git -C %s add %s"
                      worktree path ;
                    logf 1 "Wrote and added: %s" full_path ) ;
                let status =
                  Fmt.kstr System.command_to_string_list_or_fail
                    "git -C %s status --porcelain" (Path.quote worktree) in
                begin
                  match status with
                  | [] -> logf 1 "Nothing to commit."
                  | _ ->
                      logf 1 "Need to commit (%d files)" (List.length status) ;
                      let msg = Fmt.str "Update CI for %s" branch.name in
                      Fmt.kstr System.command_unit_or_fail
                        "git -C %s commit -m %s" (Path.quote worktree)
                        (Path.quote msg) ;
                      logf 1 "Commit: %S" msg ;
                      if push_flag then (
                        Fmt.kstr System.command_unit_or_fail
                          "git -C %s push %s %s:%s" (Path.quote worktree)
                          (Path.quote (Default_remote.name ()))
                          local (Path.quote branch.name) ;
                        logf 1 "PUSHED to %s / %s" (Default_remote.name ())
                          branch.name )
                      else logf 1 "Not pushing (use --push)."
                end ;
                () ) )
          ~finally:(fun () ->
            List.rev !_log
            |> List.iter ~f:(fun (depth, msg) ->
                   Fmt.pr "%s%s\n%!" (String.make (depth * 4) ' ') msg ) ) ;
        dbgf "Done." )
    $ Arg.(value (flag (info ["push"] ~doc:"All push the commits.")))
    $ const () in
  (term, info "ensure" ~doc:"Ensure that the setup is applied.")

let main () =
  let open Cmdliner in
  let version = "0.0.0" in
  let help =
    Term.
      ( ret (const (`Help (`Auto, None)))
      , info "merbocop-edit-ci" ~version ~doc:"Edit the .gitlab-ci.yml file." )
  in
  Term.exit
    ( try
        Term.eval_choice ~catch:false
          (help : unit Term.t * _)
          [show_command (); diff_command (); ensure_command ()]
      with Failure s ->
        Fmt.epr "@[<4>FAILURE:@ %a@]%!" Fmt.text s ;
        Caml.exit 3 )

let () = main ()
