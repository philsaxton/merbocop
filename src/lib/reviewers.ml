open! Base

(** Gitlab users assigned to review merge requests in accordance with their file assignment
    in a CODEOWNERS file. *)

type t = string list [@@deriving show]

(** [unique_only a b f] is `a list without any element in which (f b a) = true for
    any element of `b list. *)
let unique_only (a : 'a list) (b : 'a list) ~(equal : 'a -> 'a -> bool) :
    'a list =
  (* List.filter a ~f:(fun e -> List.for_all b ~f:(fun g -> (f g e ))) *)
  List.fold a ~init:[] ~f:(fun i a ->
      if List.exists b ~f:(equal a) then i else a :: i )

(** A list of user names who are codeowners of files which have been changed by a merge request. *)
let of_changes ~(mr : Merge_request.t) : t =
  match mr.codeowners with
  | Some codeowners ->
      List.fold mr.changes ~init:[] ~f:(fun co_reviewers change ->
          match
            List.find_map codeowners ~f:(fun co ->
                Re.(
                  let co_path = compile (Glob.glob co.path) in
                  if execp co_path change.old_path then Some co.user_names
                  else None) )
          with
          | None -> co_reviewers
          | Some owners -> owners @ co_reviewers )
      |> List.dedup_and_sort ~compare:String.compare
  | None -> []

(** Returns a list of the new reviewers to be assigned and logs Merbocop's behavior in the
    merge request report body. *)
let new_reviewers_and_report ~(mr : Merge_request.t) : t =
  let say_new = function
    | [] -> "Merbocop wouldn't have assigned any codeowners"
    | co -> Fmt.str "Merbocop would add [%s]" (String.concat ~sep:", " co) in
  let say_current = function
    | [] -> "to the MR reviewers list."
    | r ->
        Fmt.str "to the current list of reviewers: [%s]."
          (String.concat ~sep:", " r) in
  match mr.codeowners with
  | None ->
      Fmt.kstr
        (Merge_request.Reporting.add_conditional mr.reports)
        "Merbocop wouldn't have assigned any reviewers because there are no \
         codeowners. Is there a CODEOWNERS file? (Project:%d MR:%d)"
        mr.project.id mr.id ;
      []
  | Some _ ->
      let current_reviewers = mr.reviewers in
      let new_reviewers =
        unique_only (of_changes ~mr)
          (Project_members.unames (mr.author :: current_reviewers))
          ~equal:String.equal in
      Fmt.kstr
        (Merge_request.Reporting.add_conditional mr.reports)
        "%s %s (Project:%d MR:%d)" (say_new new_reviewers)
        (Project_members.unames current_reviewers |> say_current)
        mr.project.id mr.id ;
      new_reviewers

(** Call to the gitlab API which assigns reviewers to a merge request. The user_ids list
    will replace the current list of reviewers. *)
let gitlab_api ?(token : string option) ~(project : string) ~(mr_id : int)
    ~retry_options (user_ids : string list) : Jq.t =
  Web_api.get_json ~request:`PUT ?private_token:token ~retry_options
    (Gitlab.make_uri
       ~parameters:[("reviewer_ids", user_ids)]
       (Fmt.str "projects/%s/merge_requests/%d" project mr_id) )

(** Checks the Gitlab API to see if the Merge Request reviewers list has been updated before
    assinging new reviewers. *)
let race_condition ?(token : string option) ~(project : string)
    ~(mr : Merge_request.t) ~retry_options () : bool =
  let open Jq in
  let json_obj =
    get_dict
      ( Gitlab.single_merge_request ?token ~project ~mr:mr.id ~retry_options ()
      |> Bot_actions.check_json ~message:"Gitlab.single_merge_request" ) in
  match Int.equal mr.id (get_field "iid" json_obj |> get_int) with
  | false ->
      Fmt.kstr failwith
        "Failure: Reviewers.race_condition Merge Request IDs didn't match. \
         (Project:%d MR:%d)"
        mr.project.id mr.id
  | true -> (
    match
      List.equal Project_members.equal mr.reviewers
        (get_field "reviewers" json_obj |> Project_members.of_json)
    with
    | false ->
        Fmt.kstr
          (Merge_request.Reporting.add_conditional mr.reports)
          "Merbocop wouldn't have assigned any reviewers because a race \
           condition was detected (Project:%d MR:%d)"
          mr.project.id mr.id ;
        true
    | true -> false )

(** Will assign reviewers or will do nothing if reporting_only or there are not reviewers
    to assign. *)
let assign_or_report ?(token : string option) ~(project : string)
    ~(mr : Merge_request.t) ~(retry_options : Web_api.Retry_options.t)
    ~(reporting_only : bool) (reviewers : Project_members.t list) :
    (Jq.t option, exn) Result.t =
  let report_json =
    Ezjsonm.(dict [("id", int mr.id); ("reporting", `String "OK")]) in
  let json =
    if reporting_only then report_json
    else
      match reviewers with
      | [] -> report_json
      | r ->
          if race_condition ?token ~project ~mr ~retry_options () then
            report_json
          else
            let ids = Project_members.ids r |> List.map ~f:Int.to_string in
            gitlab_api
              ~token:
                (Option.value_exn
                   ~message:"Please provide --access-token to assign reviewers."
                   token )
              ~project ~mr_id:mr.id ~retry_options ids in
  try
    Ok
      (Some
         ( json
         |> Bot_actions.check_json
              ~message:
                (Printf.sprintf "Reviewers.gitlab_api project:%s MR:%d" project
                   mr.id ) ) )
  with e -> Error e

let exe token retry_options mr_list errors assign_reviewers =
  List.iter mr_list ~f:(function
    | _, Error (`Mr_not_found _) -> ()
    | project, Ok mr -> (
        let reporting_only = if assign_reviewers then false else true in
        let reviewers =
          List.concat_map (new_reviewers_and_report ~mr) ~f:(fun rev ->
              Project_members.(of_uname ~retry_options rev |> of_json) )
          @ mr.reviewers in
        match
          assign_or_report ?token ~project ~mr ~retry_options ~reporting_only
            reviewers
        with
        | Ok None | Ok (Some (_ : Jq.t)) -> ()
        | Error e ->
            errors :=
              !errors
              @ [ Fmt.str
                    "An error occurred while assigning reviewers to merge \
                     requests #%d with the following message%a\n"
                    mr.id Exn.pp e ] ) )

let cli_term () =
  let open Cmdliner.Term in
  let open Cmdliner.Arg in
  const (fun b -> `Assign_reviewers b)
  $ value
      (flag
         (info ["assign-reviewers"]
            ~doc:
              "Assign merger request reviewers based on a `CODEOWNERS` file in \
               the project's root directory. " ) )
