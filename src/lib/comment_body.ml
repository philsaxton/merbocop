open! Base

module Md = struct
  open Fmt

  let empty = ""
  let h n s = [str "%s %s" (String.make n '#') s; empty]
  let par l = l @ [empty]
  let verbatim l = "```" :: l @ ["```"]
  let plural = function 1 -> "" | _ -> "s"
  let plural_list = function [_] -> "" | _ -> "s"
  let plural_be = function 1 -> "is" | _ -> "are"
  let third_person = function 1 -> "s" | _ -> ""
  let hl = [""; String.make 10 '-'; ""]
  let colon_or_dot = function 0 -> "." | _ -> ":"

  let merge_team_doc_link =
    "http://tezos.gitlab.io/developer/contributing.html#the-merge-request-bot"

  let todo_comment_doc_link =
    "https://tezos.gitlab.io/developer/guidelines.html#todo-fixme-comments"

  let code_escape s =
    let max_consecutive_backquotes =
      String.fold s ~init:(0, 0) ~f:(fun (prev_c, prev_m) -> function
        | '`' -> (prev_c + 1, prev_m) | _ -> (0, max prev_m prev_c) )
      |> snd in
    let escaper = String.make (max_consecutive_backquotes + 1) '`' in
    String.concat ~sep:""
      [ escaper; (match s.[0] with '`' -> " " | _ -> "" | exception _ -> " "); s
      ; ( match s.[String.length s - 1] with
        | '`' -> " "
        | _ -> ""
        | exception _ -> " " ); escaper ]

  let one_couple_few nb =
    match nb with
    | 0 -> "no"
    | 1 -> "one"
    | 2 | 3 | 4 -> "a couple of"
    | _ -> "a few"
end

let pipeline_links () =
  match
    (Caml.Sys.getenv_opt "CI_PIPELINE_ID", Caml.Sys.getenv_opt "CI_JOB_ID")
  with
  | Some p, Some j ->
      Fmt.str " (CI-Pipeline [#%s](%s%s)/[%s](%s%s))" p
        "https://gitlab.com/oxheadalpha/merbocop/pipelines/" p j
        "https://gitlab.com/oxheadalpha/merbocop/-/jobs/" j
  | _, _ -> ""

let simple_message ?(with_footer = true) mr =
  let things_to_say = Analysis.warnings_of_mr mr in
  let open Fmt in
  let open Md in
  let intro = [] in
  let footer =
    if not with_footer then []
    else
      par
        [ str "------"; ""
        ; str "I, 🤖 👮, ran this inspection on `%s`."
            ( System.command_to_string_list "date -R"
            |> fst |> String.concat ~sep:"" )
        ; str
            "I usually run inspections every 6 hours and will try to edit my \
             own comment if I have any updates."
        ; str "For more information see the Tezos [documentation](%s)."
            merge_team_doc_link ] in
  let pp_code_list ppf l =
    list ~sep:(const string ", ") (fun ppf path -> pf ppf "`%s`" path) ppf l
  in
  let pp_path_list ppf l =
    let lgth = List.length l in
    let maximum = 3 in
    if lgth <= maximum then pp_code_list ppf l
    else
      let taken = List.take l maximum in
      let prefix = Caml.Filename.dirname (List.hd_exn l) in
      if List.for_all l ~f:(String.is_prefix ~prefix) then
        pf ppf "`%s/{%a, …}`" prefix
          (list ~sep:(const string ",") string)
          (List.map ~f:Caml.Filename.basename taken)
      else pf ppf "%a, …" pp_code_list taken in
  let thanks = "Thank you for your contribution 🙏" in
  let one fmt = Fmt.kstr (fun s -> [str "* %s" s]) fmt in
  let sub ~sub fmt =
    Fmt.kstr (fun s -> str "* %s" s :: List.map ~f:(str "    %s") sub) fmt in
  let so fmt = kstr (fun s -> str "* → %s" s) fmt in
  let list_of_wrong_stuff show cl =
    let nb = List.length cl in
    if nb <= 4 then List.map ~f:show cl
    else
      (List.take cl 3 |> List.map ~f:show)
      @ [so "… and there %s %d more …" (plural_be nb) (nb - 3)] in
  let list_of_wrong_commits ?(reescape = false) cl =
    let esc s = if reescape then Fmt.str "%S" s else s in
    list_of_wrong_stuff
      (fun c ->
        let open Merge_request.Commit in
        so "❌ `%s`: %s" (String.prefix c.sha 8) (code_escape (esc c.title)) )
      cl in
  let remarks =
    List.concat_map things_to_say ~f:(function
      | `Empty_description -> one "🤷 The merge-request description is empty."
      | `Almost_empty_description l ->
          one "🤷 The merge-request description is almost empty (%d bytes!)."
            l
      | `Too_long cl ->
          sub "📏 Commit messages should be ≤ %d characters:"
            Analysis.max_title_length ~sub:(list_of_wrong_commits cl)
      | `Wrong_whitespace l ->
          sub "📏 Some commit messages use wrong whitespace:"
            ~sub:(list_of_wrong_commits ~reescape:true l)
      | `Wip_commits_non_wip_mr nb ->
          one
            "👷 There %s %s commit%s tagged `WIP` or `DRAFT` while the MR is \
             **not**."
            (plural_be nb) (one_couple_few nb) (plural nb)
      | `Wrong_format cl ->
          sub
            "✍  Commit messages should match `\"Component: Imperative \
             action\"`:"
            ~sub:(list_of_wrong_commits cl)
      | `Suspect_words cl ->
          let nb = List.length cl in
          sub "👷 There %s %s commit message%s with suspect words:"
            (plural_be nb) (one_couple_few nb) (plural_list cl)
            ~sub:(list_of_wrong_commits cl)
      | `Should_allow_maintainers_to_push ->
          sub
            "⚠ You **should** edit your MR to allow maintainers to push to \
             your branch (this allows us to rebase before merging)."
            ~sub:
              [ so "See gitlab [docs](%s)"
                  "https://docs.gitlab.com/ee/user/project/merge_requests/allow_collaboration.html#enabling-commit-edits-from-upstream-members"
              ]
      | `Changes_with_forbidden_words chgs_wrds ->
          let nb = List.length chgs_wrds in
          sub "❓ There %s %s change%s that contain%s suspect words:"
            (plural_be nb) (one_couple_few nb) (plural nb) (third_person nb)
            ~sub:
              (list_of_wrong_stuff
                 (fun (chg, words) ->
                   let open Change in
                   so "`%s` contains: %s" chg.new_path
                     (List.map ~f:(str "`%S`") words |> String.concat ~sep:", ")
                   )
                 chgs_wrds )
      | `Changes_with_ng_todo_tags ng_changes ->
          let open Change in
          let nb =
            List.fold ng_changes ~init:0 ~f:(fun nb (_, tags) ->
                List.length tags + nb ) in
          let ng_tags_in_changes ((change, ng_tags) : Change.t * string list) =
            sub "`%s` contains the follwing:" change.new_path
              ~sub:(list_of_wrong_stuff (fun tag -> so "❌ `%S`" tag) ng_tags)
          in
          sub
            "❌ There %s %s change%s that contain%s nonstandard \"TODO\" or \
             \"FIXME\" comment%s:"
            (plural_be nb) (one_couple_few nb) (plural nb) (third_person nb)
            (plural nb)
            ~sub:
              ( (List.map ng_changes ~f:ng_tags_in_changes |> List.join)
              @ one
                  "These comments should match `\"TODO: <reference to issue> \
                   \"` See the tezos [documentation](%s) for more details."
                  todo_comment_doc_link )
      | `Touched_with_ng_todo_tags ng_files ->
          let nb = List.length ng_files in
          let open Repository_file in
          sub
            "❓ There %s %s touched file%s that contain%s nonstandard \
             \"TODO\" and \"FIXME\" comments:"
            (plural_be nb) (one_couple_few nb) (plural nb) (third_person nb)
            ~sub:
              ( list_of_wrong_stuff (fun s -> so "`%s`" s.file_name) ng_files
              @ one
                  "Please help standardize these as described in the tezos \
                   [documentation](%s)."
                  todo_comment_doc_link )
      | `Changes_wrong_protocol paths ->
          sub "⚠ There are changes to an *immutable* protocol:"
            ~sub:[so "%a" pp_path_list paths]
      | `Changes_proto_alpha paths ->
          sub "⚠ There are changes to proto-alpha:"
            ~sub:
              [ so "%a" pp_path_list paths
              ; so
                  "Please make sure changes are properly tagged `Proto:` and \
                   protocol reviewers are alerted." ]
      | `Changes_environment_sig paths ->
          sub "⚠ There are changes to protocol-environment signatures:"
            ~sub:
              [ so "%a" pp_path_list paths
              ; so
                  "Please make sure changes are properly tagged with `Proto:` \
                   and changes are properly coordinated with the \
                   protocol-development team." ]
      | `Changes_to_client_stuff path_list ->
          sub "⚠ There are changes to protocol-client-code:"
            ~sub:
              [ so "%a" pp_path_list path_list
              ; so
                  "Please make sure changes are properly replicated for all \
                   relevant protocols (e.g. `src/proto_alpha`, \
                   `src/proto_0XX_PtBLaBLa`, …)." ] ) in
  let meat =
    match remarks with
    | [] -> par [str "✔ Everything looks good to me! 🎉 %s" thanks]
    | _ ->
        let nb =
          List.fold remarks ~init:0 ~f:(fun x s ->
              if String.is_prefix s ~prefix:"*" then 1 + x else x ) in
        par
          ( [ str "%s, I have %s remark%s%s" thanks (one_couple_few nb)
                (plural nb) (Md.colon_or_dot nb)
              (* nb should never be 0 actually but for completeness… *); "" ]
          @ remarks ) in
  intro @ meat @ footer

let reporting (mr : Merge_request.t) =
  let open Fmt in
  let line fmt = Fmt.kstr (fun s -> [str "%s" s]) fmt in
  let label_report =
    List.concat_map (Analysis.labels_of_mr mr) ~f:(function
      | `Approvals_needed n ->
          let willdo = function
            | n when n < 0 ->
                "Merbocop would add an the `_Approved_Label_` label"
            | 0 ->
                "This MR has the exact number of apporvals required to be \
                 merged. Merbocop would add the `_Approved_Label_`"
            | 1 ->
                "This MR needs one more approval. Merbocop wouldn't add the \
                 `_Approved_Label_`"
            | _ -> "Merbocop wouldn't add the `_Approved_Label_`" in
          line "%s when used with the `--set-approved-label` option." (willdo n)
      | `Doc_only ->
          line
            "Merbocop would add a the `_Doc_Label_` label when used with the \
             `--set-doc-label` option."
      | `Not_doc_only ->
          line
            "Merbocop wouldn't add the `_Doc_Label_` when used with the \
             `--set-doc-label` option." ) in
  let reports = label_report @ mr.reports.conditionals in
  match reports with
  | [] ->
      (* This shouldn't happen *)
      [ str "Nothing to report for this merger request (Project:%d MR:%d)."
          mr.project.id mr.id ]
      @ [""]
  | _ ->
      [str "Report (Project:%d MR:%d):" mr.project.id mr.id; ""]
      @ List.concat_map reports ~f:(fun s -> [str "* %s" s])
      @ [""]

let mr_section ~ith ~mr_list (mr_res : Merge_request.mr_result) =
  let open Merge_request in
  let open Fmt in
  let open Md in
  let counter_string = str "[%d/%d] " (ith + 1) (List.length mr_list) in
  let title_style = "color: #700" in
  let block_quote_style = "border-left: solid #999 2px ; padding-left: 6px" in
  match mr_res with
  | Ok mr ->
      kstr (h 3) "%s%sMerge-Request [%s!%d](%s/merge_requests/%d)"
        counter_string
        (if mr.wip then "WIP " else "")
        mr.project.Project.path_with_namespace mr.id mr.project.Project.web_url
        mr.id
      @ (let title = str "<span style=%S>%s</span>" title_style mr.title in
         par [title]
         @
         match String.strip mr.description with
         | "" -> []
         | _ ->
             par
               [ str
                   "<details><summary>Description</summary><blockquote style=%S>\n\
                    %s\n\
                    </blockquote></details>"
                   block_quote_style mr.description ] )
      @ par ["#### Bot-Message"]
      @ simple_message ~with_footer:false mr
      @ reporting mr
  | Error (`Mr_not_found (proj, c, json, e)) ->
      kstr (h 3) "%sCommit `%s/%s`" counter_string proj c
      @ par [str "No MR found out (from open MRs in project %s)." proj]
      @ verbatim
          [ String.prefix (Jq.value_to_string ~minify:false json) 300; "..."
          ; Fmt.str "-> %a" Exn.pp e ]

let full_report ~mr_list () =
  let open Fmt in
  let open Md in
  let intro =
    par
      [ str
          "I, 🤖 👮, ran this inspection on `%s`%s. I usually run \
           inspections every 6 hours and will try to edit my own comment if I \
           have any updates."
          ( System.command_to_string_list "date -R"
          |> fst |> String.concat ~sep:"" )
          (pipeline_links ()) ]
    @ par
        [ str
            "Thank you for your \
             [contribution](https://tezos.gitlab.io/developer/contributing.html) \
             🙏" ] in
  let footer =
    [""; String.make 72 '-'; ""]
    @ par
        [ str
            "For more information about this report see the Tezos \
             [documentation](%s) and the [Merbocop](%s) project."
            merge_team_doc_link "https://gitlab.com/oxheadalpha/merbocop/" ]
  in
  let lines =
    h 2 "Merbocop Inspection Report"
    @ intro
    @ List.concat_mapi mr_list ~f:(fun ith mr ->
          try mr_section ~ith ~mr_list mr
          with e -> par [str "mr failed to make up a section: `%a`" Exn.pp e] )
    @ footer in
  String.concat ~sep:"\n" lines
