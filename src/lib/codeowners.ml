open! Base

(** A type for the repository code owners. Data is gathered from the `CODEOWNERS` file
    in the root directory of a repository. *)

type t = {path: string; user_names: string list} [@@deriving show]

(** Returns the CODEOWNERS file of the project on the master branch. *)
let repository_file ?token ~project ~retry_options () : Jq.t =
  let master = "master" in
  let file = "CODEOWNERS" in
  Repository_file.(
    of_committish ?token ~project ~retry_options ~committish:master file)

(** Content of the the CODEOWNERS file. *)
let content json : string list option =
  let open Jq in
  if mem json ["content"] then
    Some
      ( get_dict json |> get_field "content" |> get_string |> Base64.decode_exn
      |> String.split_lines )
  else None

(** A t list gathered form a CODEOWNERS file. Returns an empty list if
   no file is present *)
let of_json json : t list option =
  match content json with
  | Some con ->
      Some
        (let open List in
        filter con ~f:(fun s -> String.is_prefix s ~prefix:"/")
        |> map ~f:(String.split ~on:'@')
        |> map ~f:(function
             | [] ->
                 Fmt.failwith
                   "Codeowners.of_file Parsing Error: Empty line-list, \
                    Attempting to parse file"
             | h :: t ->
                 let path =
                   h |> String.lstrip ~drop:(Char.equal '/') |> String.strip
                 in
                 let user_names = t |> map ~f:String.strip in
                 {path; user_names} ))
  | None -> None

(** for testing JSON parsing fuctions. *)
let json_mockups : Jq.t list =
  let open Jq in
  [ (let content =
       (fun lines -> String.concat lines ~sep:"\n" |> Base64.encode_string)
         [ "# CODEOWNERS"; "# Insert code owner declarations here!"
         ; "# ************************************"
         ; "/docs                        @docs_owner @not_a_member @mr_author"
         ; "/src/lib_p2p*                @lib_p2p_owner @mr_author"
         ; "/src/proto_*/lib_benchmark   @lib_benchmark_owner @mr_author"
         ; "/.gitlab                     @gitlab_owner @mr_author"
         ; "# Octez Merge\nTeam"; "# @team.member = Team Member"; ""
         ; "* @team.member @member @member_ @team-member" ] in
     ("file_name" --> string "CODEOWNERS")
     @@@ ("ref" --> string "master")
     @@@ ("content" --> string content) )
  ; "message" --> string "404 FiLe Not Found" ]

let%expect_test "Codeowners.of_file TEST" =
  List.iteri json_mockups ~f:(fun ith j ->
      Fmt.pr "[JSON-%d:]\n%!" ith ;
      List.iter
        (Option.value ~default:[] (of_json j))
        ~f:(fun t -> Debug.expect (show t)) ) ;
  [%expect
    {|
    [JSON-0:]
       { Codeowners.path = "docs";
         user_names = ["docs_owner"; "not_a_member"; "mr_author"] }
       { Codeowners.path = "src/lib_p2p*";
         user_names = ["lib_p2p_owner"; "mr_author"] }
       { Codeowners.path = "src/proto_*/lib_benchmark";
         user_names = ["lib_benchmark_owner"; "mr_author"] }
       { Codeowners.path = ".gitlab"; user_names = ["gitlab_owner"; "mr_author"] }
    [JSON-1:] |}]
