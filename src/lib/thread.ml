open! Base

type t = {id: string; is_note: bool; comments: Comment.t list; json: Jq.t}
[@@deriving show]

(** Gitlab API call returning all discussions (threads) of a merge request.
    This will include non-thread notes which will have the field
    "individual_note= true"  *)
let merge_request_discussions ?token ~project ~mr ~retry_options () : Jq.t =
  Web_api.get_json ?private_token:token ~retry_options
    (Gitlab.make_uri
       ~parameters:[("pagination", ["keyset"]); ("per_page", ["100"])]
       (Fmt.str "projects/%s/merge_requests/%d/discussions" project mr) )

(** Gitlab API call returning the JSON result from adding a Merge request
    discussion thread. *)
let start_merge_request_discussion ?token ~project ~mr ~retry_options body :
    Jq.t =
  Web_api.get_json ~request:`POST ?private_token:token ~retry_options
    (Gitlab.make_uri
       ~parameters:[("body", [body])]
       (Fmt.str "projects/%d/merge_requests/%d/discussions/" project mr) )

(** Gitlab API call returning the JSON result from editing a merge request
    thread. *)
let edit_merge_request_discussion ?token ~project ~mr ~retry_options ~discussion
    ~note body : Jq.t =
  Debug.dbg "body: %d -> %d" (String.length body)
    (Uri.pct_encode body |> String.length) ;
  Web_api.get_json ~request:`PUT ?private_token:token ~retry_options
    (Gitlab.make_uri
       ~parameters:[("body", [body])]
       (Fmt.str "projects/%d/merge_requests/%d/discussions/%s/notes/%d" project
          mr discussion note ) )

(** A list of merge request Threads. **)
let of_json json : t list =
  let threads =
    try
      let open Jq in
      get_list
        (fun j ->
          let obj = get_dict j in
          let id = get_field "id" obj |> get_string in
          let is_note = get_field "individual_note" obj |> get_bool in
          let comments = get_field "notes" obj |> Comment.of_json in
          {id; is_note; comments; json= j} )
        json
    with e ->
      Debug.dbg "Comment.threads_of_json %s" (Jq.value_to_string json) ;
      raise e in
  List.rev_filter threads ~f:(fun thr -> not thr.is_note)

(** Transforms the command line option into an OCaml value. *)
let cli_term () =
  let open Cmdliner.Term in
  let open Cmdliner.Arg in
  const (fun b -> `Require_resolve_thread b)
  $ value
      (flag
         (info
            ["comments-require-resolve-thread"]
            ~doc:
              "Merbocop's comments will start at Gitlab discussion requiring \
               users to resolve them." ) )

(** JSON mockups for unit tests. *)
module Mockup = struct
  let thread : string -> bool -> Jq.t -> Jq.t =
   fun id b notes ->
    let open Jq in
    ("id" --> string id)
    @@@ ("individual_note" --> bool b)
    @@@ ("notes" --> notes)

  let threads : (string * bool * Jq.t) list -> Jq.t =
   fun l -> Jq.list (fun (id, b, notes) -> thread id b notes) l

  let json : Jq.t list =
    let bot = Project_members.Mockup.merbocop in
    let user = Project_members.Mockup.merbocriminal in
    let thread_notes =
      Comment.Mockup.notes
        [ (1, "Give up! I've got you surounded!", bot, false, true)
        ; (2, "Never, Copper!", user, false, true)
        ; (3, "Run for it!", user, false, true) ] in
    let single_note =
      Comment.Mockup.notes
        [ ( 987654321
          , "Basic MR comment."
          , Project_members.Mockup.merbocop
          , false
          , false ) ] in
    let system_note =
      Comment.Mockup.notes
        [ ( 123456789
          , "Basic MR System Note (e.g. a label was added)"
          , Project_members.Mockup.merbocriminal
          , true
          , false ) ] in
    [ threads
        [ ("thread-1", false, thread_notes); ("tread-2", true, single_note)
        ; ("thread-3", true, system_note) ] ]
end

let%expect_test "Threads.of_json" =
  List.iteri Mockup.json ~f:(fun ith moc ->
      Fmt.pr "[MR-%d:]\n%!" ith ;
      List.iter (of_json moc) ~f:(fun t -> Debug.expect (show t)) ) ;
  [%expect
    {|
    [MR-0:]
       { Thread.id = "thread-1"; is_note = false;
         comments =
         [{ Comment.id = 1; body = "Give up! I've got you surounded!";
            author =
            { Project_members.id = 123456; username = "merbocop";
              name = "Merge Bot Cop" };
            system = false; resolvable = true;
            json =
            `O ([("id", `Float (1.));
                  ("body", `String ("Give up! I've got you surounded!"));
                  ("author",
                   `O ([("id", `Float (123456.));
                         ("username", `String ("merbocop"));
                         ("name", `String ("Merge Bot Cop"))]));
                  ("system", `Bool (false)); ("resolvable", `Bool (true))])
            };
           { Comment.id = 2; body = "Never, Copper!";
             author =
             { Project_members.id = 654321; username = "merbocriminal";
               name = "Merge Bot Criminal" };
             system = false; resolvable = true;
             json =
             `O ([("id", `Float (2.)); ("body", `String ("Never, Copper!"));
                   ("author",
                    `O ([("id", `Float (654321.));
                          ("username", `String ("merbocriminal"));
                          ("name", `String ("Merge Bot Criminal"))]));
                   ("system", `Bool (false)); ("resolvable", `Bool (true))])
             };
           { Comment.id = 3; body = "Run for it!";
             author =
             { Project_members.id = 654321; username = "merbocriminal";
               name = "Merge Bot Criminal" };
             system = false; resolvable = true;
             json =
             `O ([("id", `Float (3.)); ("body", `String ("Run for it!"));
                   ("author",
                    `O ([("id", `Float (654321.));
                          ("username", `String ("merbocriminal"));
                          ("name", `String ("Merge Bot Criminal"))]));
                   ("system", `Bool (false)); ("resolvable", `Bool (true))])
             }
           ];
         json =
         `O ([("id", `String ("thread-1")); ("individual_note", `Bool (false));
               ("notes",
                `A ([`O ([("id", `Float (1.));
                           ("body", `String ("Give up! I've got you surounded!"));
                           ("author",
                            `O ([("id", `Float (123456.));
                                  ("username", `String ("merbocop"));
                                  ("name", `String ("Merge Bot Cop"))]));
                           ("system", `Bool (false)); ("resolvable", `Bool (true))]);
                      `O ([("id", `Float (2.));
                            ("body", `String ("Never, Copper!"));
                            ("author",
                             `O ([("id", `Float (654321.));
                                   ("username", `String ("merbocriminal"));
                                   ("name", `String ("Merge Bot Criminal"))]));
                            ("system", `Bool (false)); ("resolvable", `Bool (true))]);
                      `O ([("id", `Float (3.)); ("body", `String ("Run for it!"));
                            ("author",
                             `O ([("id", `Float (654321.));
                                   ("username", `String ("merbocriminal"));
                                   ("name", `String ("Merge Bot Criminal"))]));
                            ("system", `Bool (false)); ("resolvable", `Bool (true))])
                      ]))
               ])
         } |}]
