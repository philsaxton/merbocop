open! Base
open Ezcurl

let dumb_cache : (string, string * Ezjsonm.value) Hashtbl.t =
  Hashtbl.create (module String)

module Retry_options = struct
  type t = {retries: int; wait: float; factor: float; allow_404: bool}

  let make ?(allow_404 = false) retries wait factor : t =
    {retries; wait; factor; allow_404}

  let web_api : t = {retries= 4; wait= 0.1; factor= 2.; allow_404= false}
  let none : t = {retries= 0; wait= 0.; factor= 0.; allow_404= false}
end

(** [retry_options r f c] is an HTTP response to call [f]. Failures are retried
 **  according to [r : retry_options]. [c] is used for debuging  *)
let retry_failures ~(retry_options : Retry_options.t) ~f call : response =
  let rec retry retries wait =
    let zero_tries = Int.( <= ) retries 0 in
    let fail_message r =
      Fmt.kstr failwith
        "Web_api.retry_failures failed: Too many retries.\n\
        \ %S failed with response:%a\n"
        call pp_response r in
    match f () with
    | Error (_, s) ->
        Fmt.kstr failwith "Web_api.retry_failures failed:\n %s\n %s" call s
    | Ok res -> (
      match res.code with
      | 200 | 201 -> res
      | 404 when retry_options.allow_404 -> res
      | 429 ->
          if zero_tries then fail_message res
          else
            let rate_limit_delay =
              match
                List.find_map res.headers ~f:(fun (k, v) ->
                    if String.( = ) "retry-after" k then Some v else None )
              with
              | Some n -> Int.of_string n
              | None -> 60 - (Int.of_float (Unix.time ()) % 60) in
            Debug.dbg
              "Return code: %d --Rate limit reached. Retrying after rate limit \
               reset in %d seconds. Retries left: %d "
              res.code rate_limit_delay retries ;
            Unix.sleep rate_limit_delay ;
            retry (Int.pred retries) wait
      | _ ->
          if zero_tries then fail_message res
          else (
            Debug.dbg
              "Web_api.retry_failures returned code: %d -- Retries left: %d"
              res.code retries ;
            Unix.sleepf wait ;
            retry (Int.pred retries) (wait *. retry_options.factor) ) ) in
  retry retry_options.retries retry_options.wait

(** [get_json ?data_arg ?]*)
let get_json ?data_arg ?(request = `GET) ?(pagination = false) ?private_token
    ~(retry_options : Retry_options.t) uri : Jq.t =
  let url = Uri.to_string uri in
  let token =
    (function Some tok -> Some [("PRIVATE-TOKEN", tok)] | None -> None)
      private_token in
  let data_file =
    match data_arg with
    | None -> ("", "")
    | Some (k, v) ->
        let f = Caml.Filename.temp_file "curl-" ".data" in
        let o = Caml.open_out f in
        Caml.output_string o v ;
        Caml.close_out o ;
        (k, Fmt.str "%s" f) in
  let meth =
    match request with
    | `GET -> GET
    | `PUT -> PUT
    | `POST ->
        POST
          ( data_file
          |> function
          | "", "" -> [Curl.CURLFORM_CONTENT ("", "", Curl.DEFAULT)]
          | k, f -> [Curl.CURLFORM_FILECONTENT (k, f, Curl.DEFAULT)] ) in
  let make_cache_key url =
    match meth with
    | POST _ ->
        Fmt.str "%s %s=%S %S" (string_of_meth meth) (fst data_file)
          (snd data_file) url
    | _ ->
        Fmt.str "%s %S" (string_of_meth meth) url
        |> fun s -> String.prefix s 140 in
  match Hashtbl.find dumb_cache (make_cache_key url) with
  | Some (_, sl) -> sl
  | None ->
      let get_response meth url : response =
        let req = function
          | GET -> get ?headers:token ~url ()
          | PUT ->
              put ?headers:token ~url ~content:(`String "") ()
              (* Here ~content is required by Ezcurl but merbocop doesn't use it. *)
          | POST p -> post ?headers:token ~params:p ~url ()
          | _ ->
              Fmt.kstr failwith "%S failed. Invalid request option %s."
                (make_cache_key url) (string_of_meth meth) in
        Debug.dbg "curling --request %s" (make_cache_key url) ;
        retry_failures ~retry_options
          ~f:(fun () -> req meth)
          (make_cache_key url) in
      let all_pages_of_response meth url : response list =
        let res = get_response meth url in
        let rec all r =
          let next_page =
            try
              let links =
                List.find_map r.headers ~f:(fun (k, v) ->
                    if String.( = ) "link" k then Some v else None ) in
              let next = ">; rel=\"next\"" in
              match links with
              | None -> None
              | Some links ->
                  if String.is_substring links ~substring:next then
                    Some
                      ( String.lsplit2_exn links ~on:';'
                      |> fst
                      |> String.strip ~drop:(fun c ->
                             Char.(equal c '<' || equal c '>') ) )
                  else None
            with e ->
              Fmt.kstr failwith "Web_api.get_json Error getting next page %s"
                (Exn.to_string e) in
          match next_page with
          | None -> [r]
          | Some n -> [r] @ all (get_response meth n) in
        all res in
      let json =
        try
          let open Ezjsonm in
          if pagination then
            let lines =
              all_pages_of_response meth url |> List.map ~f:(fun p -> p.body)
            in
            List.concat_map lines ~f:(fun body ->
                value_from_string body |> get_list (fun v -> v) )
            |> list (fun v -> v)
          else
            let line = (get_response meth url).body in
            Ezjsonm.value_from_string line
        with Ezjsonm.Parse_error (v, e) ->
          Fmt.kstr failwith "Error parsing response: %s, %S" e
            (Ezjsonm.value_to_string v) in
      let mnemonic =
        make_cache_key url |> String.map ~f:(function '/' -> '_' | c -> c) in
      Hashtbl.add dumb_cache ~key:(make_cache_key url) ~data:(mnemonic, json)
      |> ignore ;
      json

let dump_cache ~path =
  let ith = ref 0 in
  let hash s = Caml.Digest.(string s |> to_hex) in
  Fmt.kstr System.command_to_string_list "mkdir -p %s" path |> ignore ;
  Debug.dbg "Dumping-cache to %s (%d entries)" path (Hashtbl.length dumb_cache) ;
  Hashtbl.iteri dumb_cache ~f:(fun ~key ~data:(mnemo, json) ->
      Caml.incr ith ;
      let o = Caml.open_out (Fmt.str "%s/%03x-%s" path !ith (hash mnemo)) in
      Ezjsonm.value_to_channel ~minify:false o
        Ezjsonm.(
          dict
            [("key", `String key); ("mnemonic", `String mnemo); ("json", json)]) ;
      Caml.close_out o )

let load_cache ~path =
  try
    let files = Caml.Sys.readdir path |> Array.to_list in
    List.iter files ~f:(fun fname ->
        try
          let i = Fmt.kstr Caml.open_in_bin "%s/%s" path fname in
          match Ezjsonm.value_from_channel i with
          | `O
              [ ("key", `String key); ("mnemonic", `String mnemonic)
              ; ("json", json) ] ->
              Caml.close_in i ;
              Hashtbl.add dumb_cache ~key ~data:(mnemonic, json) |> ignore
          | json ->
              Caml.close_in i ;
              Fmt.kstr failwith "Wrong JSNO: %s" (Ezjsonm.value_to_string json)
        with e -> Debug.dbg "Error loading cache: %a" Exn.pp e )
  with e -> Debug.dbg "Not loading cache at all: %a" Exn.pp e
