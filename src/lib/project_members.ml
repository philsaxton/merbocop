open! Base

(** A type for project_members. *)
type t = {id: int; username: string; name: string} [@@deriving show]

let equal pm1 pm2 : bool =
  Int.equal pm1.id pm2.id && String.equal pm1.username pm2.username

let compare pm1 pm2 : int = Int.compare pm1.id pm2.id

let ids (pm : t list) : int list =
  List.fold pm ~init:[] ~f:(fun i r -> r.id :: i)

let unames (pm : t list) : string list =
  List.fold pm ~init:[] ~f:(fun i r -> r.username :: i)

(** Gitlab API call to query a user. *)
let of_uname ~retry_options (user_name : string) : Jq.t =
  Web_api.get_json ~request:`GET ~retry_options
    (Gitlab.make_uri ~parameters:[("username", [user_name])] (Fmt.str "users"))

(** Gitlab API call to returning all project users. *)
let all ?token ~project ~retry_options () : Jq.t =
  Web_api.get_json ~request:`GET ?private_token:token ~retry_options
    (Gitlab.make_uri (Fmt.str "projects/%s/members" project))

(** Parse JSON from Gitlab query for a single user. *)
let user_of_json json : t =
  let open Jq in
  try
    let dict = get_dict json in
    let id = get_field "id" dict |> get_int in
    let username = get_field "username" dict |> get_string in
    let name = get_field "name" dict |> get_string in
    {id; username; name}
  with e ->
    Debug.dbg "Failed to get Project_members with output:\n`%s`\n`%s`\n"
      (Exn.to_string_mach e) (value_to_string json) ;
    raise e

(** Parse json from Gitlab users query. *)
let of_json json : t list =
  let open Jq in
  try get_list (fun v -> user_of_json v) json
  with e ->
    Debug.dbg "Failed to get Project_members with output:\n`%s`\n`%s`\n"
      (Exn.to_string_mach e) (value_to_string json) ;
    raise e

(** JSON mockups for unit tests. *)
module Mockup = struct
  let user : int -> string -> string -> Jq.t =
    let open Jq in
    fun i u n ->
      ("id" --> int i) @@@ ("username" --> string u) @@@ ("name" --> string n)

  let users : (int * string * string) list -> Jq.t =
   fun l -> Jq.list (fun (i, u, n) -> user i u n) l

  let merbocop = user 123456 "merbocop" "Merge Bot Cop"
  let merbocriminal = user 654321 "merbocriminal" "Merge Bot Criminal"
end

let%expect_test "Project_members" =
  user_of_json Mockup.merbocop |> show |> Debug.expect ;
  [%expect
    {|
      { Project_members.id = 123456; username = "merbocop"; name = "Merge Bot Cop"
        } |}] ;
  let mock_json =
    Mockup.users
      [ (123456, "moe", "Moe the Stooge"); (234567, "curly", "Curly the Stooge")
      ; (345678, "larry", "Larry the Stooge") ] in
  List.iter (of_json mock_json) ~f:(fun t -> Debug.expect (show t)) ;
  [%expect
    {|
    { Project_members.id = 123456; username = "moe"; name = "Moe the Stooge" }
    { Project_members.id = 234567; username = "curly"; name = "Curly the Stooge"
      }
    { Project_members.id = 345678; username = "larry"; name = "Larry the Stooge"
      } |}]
