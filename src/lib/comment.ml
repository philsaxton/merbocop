open! Base

type t =
  { id: int
  ; body: string
  ; author: Project_members.t
  ; system: bool
  ; resolvable: bool
  ; json: Jq.t }
[@@deriving show]

(** Gitlab API call returning all notes (comments) of a merge request
    This will include dissussion notes (comments on threads) which
    have the feild "resolvable= true"  *)
let merge_request_notes ?token ~project ~mr ~retry_options () : Jq.t =
  Web_api.get_json ?private_token:token ~retry_options
    (Gitlab.make_uri
       ~parameters:[("pagination", ["keyset"]); ("per_page", ["100"])]
       (Fmt.str "projects/%s/merge_requests/%d/notes" project mr) )

(** Gitlab API call returning the JSON result from adding a Merge Request
    note. *)
let add_merge_request_note ?token ~project ~mr ~retry_options body : Jq.t =
  Web_api.get_json ~request:`POST ?private_token:token ~retry_options
    (Gitlab.make_uri
       ~parameters:[("body", [body])]
       (Fmt.str "projects/%d/merge_requests/%d/notes" project mr) )

(** Gitlab API call returning the JSON result from editing a merge request
    note. *)
let edit_merge_request_note ?token ~project ~mr ~retry_options ~note body : Jq.t
    =
  Debug.dbg "body: %d -> %d" (String.length body)
    (Uri.pct_encode body |> String.length) ;
  Web_api.get_json ~request:`PUT ?private_token:token ~retry_options
    (Gitlab.make_uri
       ~parameters:[("body", [body])]
       (Fmt.str "projects/%d/merge_requests/%d/notes/%d" project mr note) )

(** Parses JSON from the Gitlab API call to get merge request notes (comments). *)
let of_json json : t list =
  try
    let open Jq in
    get_list
      (fun j ->
        let obj = get_dict j in
        let id = get_field "id" obj |> get_int in
        (* let thread_note = *)
        (*   String.equal (get_field "type" obj |> get_string) "DiscussionNote" *)
        (* in *)
        let body = get_field "body" obj |> get_string in
        let author = get_field "author" obj |> Project_members.user_of_json in
        let system = get_field "system" obj |> get_bool in
        let resolvable = get_field "resolvable" obj |> get_bool in
        {id; (* thread_note; *) body; author; system; resolvable; json= j} )
      json
  with e ->
    Debug.dbg "Comment.of_json %s\n" (Jq.value_to_string json) ;
    raise e

(** JSON Mockups for unitests tests. *)
module Mockup = struct
  let note : int -> string -> Jq.t -> bool -> bool -> Jq.t =
    let open Jq in
    fun i b a s r ->
      ("id" --> int i)
      @@@ ("body" --> string b)
      @@@ ("author" --> a)
      @@@ ("system" --> bool s)
      @@@ ("resolvable" --> bool r)

  let notes : (int * string * Jq.t * bool * bool) list -> Jq.t =
   fun l -> Jq.list (fun (i, b, a, s, r) -> note i b a s r) l

  let json : Jq.t list =
    [ notes
        [ ( 987654321
          , "Basic MR comment."
          , Project_members.Mockup.merbocop
          , false
          , false )
        ; ( 123456789
          , "Basic MR System Note (e.g. a label was added)"
          , Project_members.Mockup.merbocriminal
          , true
          , false ) ] ]
end

let%expect_test "Comment.of_json" =
  List.iteri Mockup.json ~f:(fun ith moc ->
      Fmt.pr "[MR-%d:]\n%!" ith ;
      List.iter (of_json moc) ~f:(fun t -> Debug.expect (show t)) ) ;
  [%expect
    {|
    [MR-0:]
       { Comment.id = 987654321; body = "Basic MR comment.";
         author =
         { Project_members.id = 123456; username = "merbocop";
           name = "Merge Bot Cop" };
         system = false; resolvable = false;
         json =
         `O ([("id", `Float (987654321.)); ("body", `String ("Basic MR comment."));
               ("author",
                `O ([("id", `Float (123456.)); ("username", `String ("merbocop"));
                      ("name", `String ("Merge Bot Cop"))]));
               ("system", `Bool (false)); ("resolvable", `Bool (false))])
         }
       { Comment.id = 123456789;
         body = "Basic MR System Note (e.g. a label was added)";
         author =
         { Project_members.id = 654321; username = "merbocriminal";
           name = "Merge Bot Criminal" };
         system = true; resolvable = false;
         json =
         `O ([("id", `Float (123456789.));
               ("body", `String ("Basic MR System Note (e.g. a label was added)"));
               ("author",
                `O ([("id", `Float (654321.));
                      ("username", `String ("merbocriminal"));
                      ("name", `String ("Merge Bot Criminal"))]));
               ("system", `Bool (true)); ("resolvable", `Bool (false))])
         } |}]
