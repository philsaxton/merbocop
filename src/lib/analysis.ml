open! Base

let diff_adds diff ~substring =
  let lines = String.split diff ~on:'\n' in
  let substring = String.uppercase substring in
  let _, p =
    List.fold ~init:(0, 0) lines ~f:(fun (minuses, pluses) line ->
        match line.[0] with
        | '-' ->
            if String.is_substring (String.uppercase line) ~substring then
              (minuses + 1, pluses)
            else (minuses, pluses)
        | '+' ->
            if String.is_substring (String.uppercase line) ~substring then
              if minuses = 0 then (0, pluses + 1) else (minuses - 1, pluses)
            else (minuses, pluses)
        | exception _ ->
            (* This is pure context, we're between diffs *) (0, pluses)
        | _ -> (* This is pure context, we're between diffs *) (0, pluses) )
  in
  p > 0

let re_proto_0 = Re.Posix.compile_pat "src/proto_0.*/lib_protocol"
let re_proto_alpha = Re.Posix.compile_pat "src/proto_alpha.*/lib_protocol"
let re_proto_test = Re.Posix.compile_pat "src/proto_.*/lib_protocol/test/"

let client_proto_dirs =
  [ "bin_accuser"; "bin_baker"; "lib_client"; "lib_client_commands"
  ; "lib_client_sapling"; "lib_delegate"; "lib_injector"; "lib_plugin"
  ; "lib_parameters" ]

let or_empty c l = if c then l else []
let on_non_empty l ~f = if Poly.(l = []) then [] else [f l]

let changes_touching_protocol changes =
  let open Change in
  let check_path change predicate =
    or_empty (predicate change.old_path) [change.old_path]
    @ or_empty (predicate change.new_path) [change.new_path] in
  let changes_wrong_protocol change =
    List.dedup_and_sort ~compare:String.compare
      (check_path change (fun p ->
           Re.execp re_proto_0 p && not (Re.execp re_proto_test p) ) ) in
  let changes_proto_alpha change =
    check_path change (fun p ->
        Re.execp re_proto_alpha p && not (Re.execp re_proto_test p) ) in
  let changes_to_any_client change =
    List.concat_map client_proto_dirs ~f:(fun dir ->
        check_path change (fun p ->
            String.is_substring p ~substring:"src/proto_"
            && String.is_substring p ~substring:dir ) ) in
  let dedup_strings = List.dedup_and_sort ~compare:String.compare in
  on_non_empty
    (dedup_strings (List.concat_map changes ~f:changes_wrong_protocol))
    ~f:(fun l -> `Changes_wrong_protocol l)
  @ on_non_empty
      (dedup_strings (List.concat_map changes ~f:changes_proto_alpha))
      ~f:(fun l -> `Changes_proto_alpha l)
  @ on_non_empty
      (dedup_strings (List.concat_map changes ~f:changes_to_any_client))
      ~f:(fun l -> `Changes_to_client_stuff l)

let environment_sigs_location =
  Re.Posix.compile_pat "src/lib_protocol_environment/sigs/v[01]/.*"

let changes_touching_environment_sigs changes =
  let open Change in
  List.concat_map changes ~f:(fun change ->
      let one_of_them p =
        if Re.execp environment_sigs_location p then [p] else [] in
      one_of_them change.old_path @ one_of_them change.new_path )
  |> List.dedup_and_sort ~compare:String.compare
  |> on_non_empty ~f:(fun more -> `Changes_environment_sig more)

let max_title_length = 70

let commits_of_mr mr =
  let open Merge_request in
  let re = Re.Posix.compile_pat "[a-zA-Z0-9\\-\\_/]+:.*" in
  let wiptag = Re.Posix.compile_pat {|(WIP|DRAFT)|} in
  let suspects = Re.Posix.compile_pat {|(FIX ?UP|SKIP CI)|} in
  let change_if value f cond = if cond then f value else value in
  let wips, non_matches, too_longs, with_whitespace, suspect_words =
    List.fold mr.commits ~init:(0, [], [], [], [])
      ~f:(fun
           (wips, non_matches, too_longs, with_whitespace, suspect_words)
           ({Commit.title; _} as c)
         ->
        let add_commit l = c :: l in
        let has_wrong_whitespace =
          match String.split_on_chars title ~on:['\t'; '\n'] with
          | [_] -> false
          | _ -> true in
        let _, consecutive_spaces =
          String.fold title ~init:(false, 0) ~f:(fun (previs, n) -> function
            | ' ' when previs -> (true, n + 1)
            | ' ' -> (true, n)
            | _ -> (false, n) ) in
        ( change_if wips Caml.succ (String.uppercase title |> Re.execp wiptag)
        , change_if non_matches add_commit (not (Re.execp re title))
        , change_if too_longs add_commit (String.length title > max_title_length)
        , change_if with_whitespace add_commit
            Poly.(
              consecutive_spaces >= 1 || has_wrong_whitespace
              || String.strip title <> title)
        , change_if suspect_words add_commit
            (String.uppercase title |> Re.execp suspects) ) ) in
  object
    method wips = wips

    method wrong_commit_messages =
      on_non_empty non_matches ~f:(fun n -> `Wrong_format n)
      @ on_non_empty too_longs ~f:(fun l -> `Too_long l)
      @ on_non_empty with_whitespace ~f:(fun l -> `Wrong_whitespace l)
      @ on_non_empty suspect_words ~f:(fun l -> `Suspect_words l)
  end

let maintainer_access mr =
  let open Merge_request in
  match mr.allow_maintainer_to_push with
  | Some true -> []
  | Some false -> [`Should_allow_maintainers_to_push]
  | None -> []

let description mr =
  match String.length mr.Merge_request.description with
  | 0 -> [`Empty_description]
  | l when l < 8 -> [`Almost_empty_description l]
  | _ -> []

let todotag = Re.(Posix.compile_pat {|(TODO|FIXME)|})

let todostandard =
  Re.(
    Posix.compile_pat
      {|\(\* ?(TODO|FIXME):? ((https://gitlab.com/.*/-/issues/[0-9]*)|([a-zA-Z-/_]*#[0-9]*))|})

(** [todo_tags_of_touched_files touched_files] is a warning list,
      \[>`Touched_with_ng_tod_tags of ng_files\] list.
      [ng_files] is a Merge_request.File.t list of files that contain
      nonstandard TODO or FIXME found in [touched_files]. *)
let todo_tags_of_touched_files (touched_files : Repository_file.t list) =
  let open Re in
  let open Repository_file in
  match
    List.filter touched_files ~f:(fun file ->
        List.exists file.content ~f:(fun line ->
            String.uppercase line |> execp todotag
            && not (execp todostandard line) ) )
    |> List.dedup_and_sort ~compare:(fun a b ->
           String.compare a.file_name b.file_name )
  with
  | [] -> []
  | ng_files -> [`Touched_with_ng_todo_tags ng_files]

(** [todo_tags_of_changes changes] is a warning list
      \[> `Changes_with_ng_todo_tags ng_change\] list.
      [ng_change] is a (Change.t * ng_tags list) list
      [ng_tags] is a string list of lines of the merge request diff
      that contain nonstandard TODO or FIXME comments found in [changes]. *)
let todo_tags_of_changes (changes : Change.t list) =
  let open Re in
  match
    List.fold changes ~init:[] ~f:(fun ng_changes change ->
        match
          List.exists ["TODO"; "FIXME"] ~f:(fun tag ->
              diff_adds change.diff ~substring:tag )
        with
        | false -> ng_changes
        | true -> (
            let lines = change.diff |> String.split_lines in
            match
              List.filter lines ~f:(fun line ->
                  String.uppercase line |> execp todotag
                  && not (execp todostandard line) )
            with
            | [] -> ng_changes
            | ng_tags -> (change, ng_tags) :: ng_changes ) )
  with
  | [] -> []
  | ng_changes -> [`Changes_with_ng_todo_tags ng_changes]

(** [doc_only mr] is [>`Doc_ony] if the [mr] changes
   **  .rst and .md files only, and [>`Not_doc_only] otherwise. *)
let doc_only (mr : Merge_request.t) =
  let open Merge_request in
  if
    List.for_all mr.changes ~f:(fun c ->
        List.exists
          ~f:(fun suffix ->
            String.is_suffix (Caml.Filename.basename c.old_path) ~suffix )
          [".rst"; ".md"] )
  then [`Doc_only]
  else [`Not_doc_only]

(** [approvals_needed mr] is the number of approvals required before a
   **  [mr] can be merged. A number greater than 1 or less than 0
   **  tells Merbocop that un/approving the [mr] will not change
   **  the approved status while applying/removing the "approved" label. *)
let approvals_needed mr =
  let open Merge_request in
  let n =
    mr.approvals.approvals_required - List.length mr.approvals.approved_by in
  [`Approvals_needed n]

(** [labels_of_mr mr] is a list of label instructions : ploymophic varaiants
   **  to be appled to the [mr]. *)
let labels_of_mr mr = approvals_needed mr @ doc_only mr

let warnings_of_mr mr =
  let open Merge_request in
  ( match
      List.fold mr.changes ~init:[] ~f:(fun chgs_wrds chg ->
          let res =
            List.filter ["WIP"] ~f:(fun substring ->
                diff_adds chg.diff ~substring ) in
          match res with [] -> chgs_wrds | more -> (chg, more) :: chgs_wrds )
    with
  | [] -> []
  | m -> [`Changes_with_forbidden_words m] )
  @ todo_tags_of_changes mr.changes
  @ todo_tags_of_touched_files mr.touched_files
  @ changes_touching_protocol mr.changes
  @ maintainer_access mr
  @
  let commits_analysis = commits_of_mr mr in
  ( if mr.wip || commits_analysis#wips = 0 then []
  else [`Wip_commits_non_wip_mr commits_analysis#wips] )
  @ commits_analysis#wrong_commit_messages @ description mr
  @ changes_touching_environment_sigs mr.changes
