open! Base

type t =
  { id: int
  ; path_with_namespace: string
  ; http_url_to_repo: string
  ; web_url: string
  ; json: Jq.t }
[@@deriving show]

let by_id ?token project_id ~retry_options : Jq.t =
  Web_api.get_json ?private_token:token ~retry_options
    (Gitlab.make_uri (Fmt.str "projects/%d" project_id))

let of_json json : t =
  let open Jq in
  let field f name =
    try get_dict json |> get_field name |> f
    with e ->
      Fmt.kstr failwith "Project.of_id get_field:`%s`@,%s@,%s@," name
        (Exn.to_string_mach e) (Jq.value_to_string json) in
  let id = field get_int "id" in
  let path_with_namespace = field get_string "path_with_namespace" in
  let http_url_to_repo = field get_string "http_url_to_repo" in
  let web_url = field get_string "web_url" in
  {id; path_with_namespace; http_url_to_repo; web_url; json}

let of_id ?token ~retry_options project_id : t =
  let json = by_id ?token project_id ~retry_options in
  let p = of_json json in
  if p.id = project_id then p
  else
    Fmt.kstr failwith "Project.by_id project_id: %d did not match: `%d`@,%s@,"
      project_id p.id (Jq.value_to_string json)
