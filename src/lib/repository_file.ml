open! Base

(** A type for repository files. *)

type t =
  {json: Jq.t; committish: string; file_name: string; content: string list}
[@@deriving show]

(** [of_referenc] is an Jq.t resulting from a Gitlab API call for a [project] file at [path].
      [committish] should be a branch name, tag or commit number of type string *)
let of_committish ?(token : string option) ~(project : string) ~retry_options
    ~(committish : string) (path : string) : Jq.t =
  let path = Uri.pct_encode path in
  Web_api.get_json ?private_token:token ~retry_options
    (Gitlab.make_uri
       ~parameters:[("ref", [committish])]
       (Fmt.str "projects/%s/repository/files/%s" project path) )

(** Parse json form Gilab file query : t   *)
let of_json json_obj : t =
  let open Jq in
  try
    let dict = get_dict json_obj in
    let committish = get_field "ref" dict |> get_string in
    let file_name = get_field "file_name" dict |> get_string in
    let content =
      get_field "content" dict |> get_string |> Base64.decode_exn
      |> String.split_lines in
    {json= json_obj; committish; file_name; content}
  with e ->
    Debug.dbg "Repository_file failed with output:\n`%s`\n`%s`\n"
      (Exn.to_string_mach e)
      (Jq.value_to_string json_obj) ;
    raise e

(** mock_json : Jq.t for testing JSON parsing fuctions. *)
let mock_json : Jq.t =
  let open Jq in
  let content =
    "T3BlbiBTb3VyY2UgTGljZW5zZQpDb3B5cmlnaHQgKGMpIDIwMTggRHluYW1pYyBMZWRnZXIgU29sdXRpb25zLCBJbmMuIDxjb250YWN0QHRlem9zLmNvbT4KQ29weXJpZ2h0IChjKSAyMDE4LTIwMjAgTm9tYWRpYyBMYWJzIDxjb250YWN0QG5vbWFkaWMtbGFicy5jb20+CkNvcHlyaWdodCAoYykgMjAxOS0yMDIwIE1ldGFzdGF0ZSBBRyA8aGVsbG9AbWV0YXN0YXRlLmNoPgpDb3B5cmlnaHQgKGMpIDIwMTgtMjAyMCBUYXJpZGVzIDxjb250YWN0QHRhcmlkZXMuY29tPgoKUGVybWlzc2lvbiBpcyBoZXJlYnkgZ3JhbnRlZCwgZnJlZSBvZiBjaGFyZ2UsIHRvIGFueSBwZXJzb24gb2J0YWluaW5nIGEKY29weSBvZiB0aGlzIHNvZnR3YXJlIGFuZCBhc3NvY2lhdGVkIGRvY3VtZW50YXRpb24gZmlsZXMgKHRoZSAiU29mdHdhcmUiKSwKdG8gZGVhbCBpbiB0aGUgU29mdHdhcmUgd2l0aG91dCByZXN0cmljdGlvbiwgaW5jbHVkaW5nIHdpdGhvdXQgbGltaXRhdGlvbgp0aGUgcmlnaHRzIHRvIHVzZSwgY29weSwgbW9kaWZ5LCBtZXJnZSwgcHVibGlzaCwgZGlzdHJpYnV0ZSwgc3VibGljZW5zZSwKYW5kL29yIHNlbGwgY29waWVzIG9mIHRoZSBTb2Z0d2FyZSwgYW5kIHRvIHBlcm1pdCBwZXJzb25zIHRvIHdob20gdGhlClNvZnR3YXJlIGlzIGZ1cm5pc2hlZCB0byBkbyBzbywgc3ViamVjdCB0byB0aGUgZm9sbG93aW5nIGNvbmRpdGlvbnM6CgpUaGUgYWJvdmUgY29weXJpZ2h0IG5vdGljZSBhbmQgdGhpcyBwZXJtaXNzaW9uIG5vdGljZSBzaGFsbCBiZSBpbmNsdWRlZAppbiBhbGwgY29waWVzIG9yIHN1YnN0YW50aWFsIHBvcnRpb25zIG9mIHRoZSBTb2Z0d2FyZS4KClRIRSBTT0ZUV0FSRSBJUyBQUk9WSURFRCAiQVMgSVMiLCBXSVRIT1VUIFdBUlJBTlRZIE9GIEFOWSBLSU5ELCBFWFBSRVNTIE9SCklNUExJRUQsIElOQ0xVRElORyBCVVQgTk9UIExJTUlURUQgVE8gVEhFIFdBUlJBTlRJRVMgT0YgTUVSQ0hBTlRBQklMSVRZLApGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRSBBTkQgTk9OSU5GUklOR0VNRU5ULiBJTiBOTyBFVkVOVCBTSEFMTApUSEUgQVVUSE9SUyBPUiBDT1BZUklHSFQgSE9MREVSUyBCRSBMSUFCTEUgRk9SIEFOWSBDTEFJTSwgREFNQUdFUyBPUiBPVEhFUgpMSUFCSUxJVFksIFdIRVRIRVIgSU4gQU4gQUNUSU9OIE9GIENPTlRSQUNULCBUT1JUIE9SIE9USEVSV0lTRSwgQVJJU0lORwpGUk9NLCBPVVQgT0YgT1IgSU4gQ09OTkVDVElPTiBXSVRIIFRIRSBTT0ZUV0FSRSBPUiBUSEUgVVNFIE9SIE9USEVSCkRFQUxJTkdTIElOIFRIRSBTT0ZUV0FSRS4K"
  in
  ("file_name" --> string "LICENSE")
  @@@ ("ref" --> string "master")
  @@@ ("content" --> string content)

let%expect_test "Repository_file.of_json TEST" =
  of_json mock_json |> pp Fmt.stdout ;
  [%expect
    {|
    { Repository_file.json =
      `O ([("file_name", `String ("LICENSE")); ("ref", `String ("master"));
            ("content",
             `String ("T3BlbiBTb3VyY2UgTGljZW5zZQpDb3B5cmlnaHQgKGMpIDIwMTggRHluYW1pYyBMZWRnZXIgU29sdXRpb25zLCBJbmMuIDxjb250YWN0QHRlem9zLmNvbT4KQ29weXJpZ2h0IChjKSAyMDE4LTIwMjAgTm9tYWRpYyBMYWJzIDxjb250YWN0QG5vbWFkaWMtbGFicy5jb20+CkNvcHlyaWdodCAoYykgMjAxOS0yMDIwIE1ldGFzdGF0ZSBBRyA8aGVsbG9AbWV0YXN0YXRlLmNoPgpDb3B5cmlnaHQgKGMpIDIwMTgtMjAyMCBUYXJpZGVzIDxjb250YWN0QHRhcmlkZXMuY29tPgoKUGVybWlzc2lvbiBpcyBoZXJlYnkgZ3JhbnRlZCwgZnJlZSBvZiBjaGFyZ2UsIHRvIGFueSBwZXJzb24gb2J0YWluaW5nIGEKY29weSBvZiB0aGlzIHNvZnR3YXJlIGFuZCBhc3NvY2lhdGVkIGRvY3VtZW50YXRpb24gZmlsZXMgKHRoZSAiU29mdHdhcmUiKSwKdG8gZGVhbCBpbiB0aGUgU29mdHdhcmUgd2l0aG91dCByZXN0cmljdGlvbiwgaW5jbHVkaW5nIHdpdGhvdXQgbGltaXRhdGlvbgp0aGUgcmlnaHRzIHRvIHVzZSwgY29weSwgbW9kaWZ5LCBtZXJnZSwgcHVibGlzaCwgZGlzdHJpYnV0ZSwgc3VibGljZW5zZSwKYW5kL29yIHNlbGwgY29waWVzIG9mIHRoZSBTb2Z0d2FyZSwgYW5kIHRvIHBlcm1pdCBwZXJzb25zIHRvIHdob20gdGhlClNvZnR3YXJlIGlzIGZ1cm5pc2hlZCB0byBkbyBzbywgc3ViamVjdCB0byB0aGUgZm9sbG93aW5nIGNvbmRpdGlvbnM6CgpUaGUgYWJvdmUgY29weXJpZ2h0IG5vdGljZSBhbmQgdGhpcyBwZXJtaXNzaW9uIG5vdGljZSBzaGFsbCBiZSBpbmNsdWRlZAppbiBhbGwgY29waWVzIG9yIHN1YnN0YW50aWFsIHBvcnRpb25zIG9mIHRoZSBTb2Z0d2FyZS4KClRIRSBTT0ZUV0FSRSBJUyBQUk9WSURFRCAiQVMgSVMiLCBXSVRIT1VUIFdBUlJBTlRZIE9GIEFOWSBLSU5ELCBFWFBSRVNTIE9SCklNUExJRUQsIElOQ0xVRElORyBCVVQgTk9UIExJTUlURUQgVE8gVEhFIFdBUlJBTlRJRVMgT0YgTUVSQ0hBTlRBQklMSVRZLApGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRSBBTkQgTk9OSU5GUklOR0VNRU5ULiBJTiBOTyBFVkVOVCBTSEFMTApUSEUgQVVUSE9SUyBPUiBDT1BZUklHSFQgSE9MREVSUyBCRSBMSUFCTEUgRk9SIEFOWSBDTEFJTSwgREFNQUdFUyBPUiBPVEhFUgpMSUFCSUxJVFksIFdIRVRIRVIgSU4gQU4gQUNUSU9OIE9GIENPTlRSQUNULCBUT1JUIE9SIE9USEVSV0lTRSwgQVJJU0lORwpGUk9NLCBPVVQgT0YgT1IgSU4gQ09OTkVDVElPTiBXSVRIIFRIRSBTT0ZUV0FSRSBPUiBUSEUgVVNFIE9SIE9USEVSCkRFQUxJTkdTIElOIFRIRSBTT0ZUV0FSRS4K"))
            ]);
      committish = "master"; file_name = "LICENSE";
      content =
      ["Open Source License";
        "Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>";
        "Copyright (c) 2018-2020 Nomadic Labs <contact@nomadic-labs.com>";
        "Copyright (c) 2019-2020 Metastate AG <hello@metastate.ch>";
        "Copyright (c) 2018-2020 Tarides <contact@tarides.com>"; "";
        "Permission is hereby granted, free of charge, to any person obtaining a";
        "copy of this software and associated documentation files (the \"Software\"),";
        "to deal in the Software without restriction, including without limitation";
        "the rights to use, copy, modify, merge, publish, distribute, sublicense,";
        "and/or sell copies of the Software, and to permit persons to whom the";
        "Software is furnished to do so, subject to the following conditions:";
        "";
        "The above copyright notice and this permission notice shall be included";
        "in all copies or substantial portions of the Software."; "";
        "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR";
        "IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,";
        "FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL";
        "THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER";
        "LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING";
        "FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER";
        "DEALINGS IN THE SOFTWARE."]
      } |}]
