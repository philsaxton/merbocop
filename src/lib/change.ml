open! Base

(** A type merger request changes. *)

type t =
  { json: Jq.t
  ; sha: string
  ; old_path: string
  ; new_path: string
  ; deleted_file: bool
  ; diff: string }
[@@deriving show]

(** [of_merge_request] is JSON : Jq.t resulting from a Gitlab API call for all changes
      found in a specifict merge request [mr].  *)
let of_merge_request ?token ~project ~mr ~retry_options () =
  Web_api.get_json ?private_token:token ~retry_options
    (Gitlab.make_uri ~parameters:[]
       (Fmt.str "projects/%s/merge_requests/%d/changes" project mr) )

(** [of_json] is t parsed from JSON data : Jq.t *)
let of_json : Jq.t -> t list =
 fun json ->
  let open Jq in
  try
    let sha = get_dict json |> get_field "sha" |> get_string in
    get_dict json |> get_field "changes"
    |> get_list (fun j ->
           let dict = get_dict j in
           let old_path = get_field "old_path" dict |> get_string in
           let new_path = get_field "new_path" dict |> get_string in
           let deleted_file = get_field "deleted_file" dict |> get_bool in
           let diff = get_field "diff" dict |> get_string in
           {json; sha; old_path; new_path; deleted_file; diff} )
  with e ->
    Debug.dbg "Change.of_json failed with output\n`%s`\n`%s`\n"
      (Exn.to_string_mach e) (value_to_string json) ;
    raise e

(** json_mocups : Jq.t list for testing JSON parsing fuctions. *)
let json_mockups : Jq.t list =
  let open Jq in
  let base =
    [ ("old_path", string "some/old/path"); ("new_path", string "some/new/path")
    ; ("deleted_file", bool false); ("diff", string "some diff") ] in
  let change : string -> string -> bool -> (string * value) list =
   fun o n b ->
    [ ("old_path", string o); ("new_path", string n); ("deleted_file", bool b)
    ; ("diff", string "some diff") ] in
  let json : string -> (string * value) list list -> Jq.t =
   fun sha chan ->
    ("sha" --> string sha) @@@ ("changes" --> list dict (base :: chan)) in
  [ json "Mock MR 1"
      [ change "docs" "docs" false
      ; change "src/lib_p2p/p2p" "src/lib_p2p/p2p" false ]
  ; json "Mock MR 2"
      [ change "src/lib_p2p_services/p2p_errors.ml"
          "src/lib_p2p_services/p2p_errors.ml" false ]
  ; json "Mock MR 3"
      [ change "src/proto_013_PtJakart/lib_benchmark/autocomp.ml"
          "src/proto_013_PtJakart/lib_benchmark/autocomp.ml" false
      ; change "src/proto_014_PtKathma/lib_benchmark/execution_context.ml"
          "src/proto_014_PtKathma/lib_benchmark/execution_context.ml" true ]
  ; json "Mock MR 4" [change ".gitlab" "" true] ]

let%expect_test "Change.of_json Test" =
  List.iteri json_mockups ~f:(fun ith moc ->
      Fmt.pr "[JSON-%d:]\n%!" ith ;
      List.iter (of_json moc) ~f:(fun t -> Debug.expect (show t)) ) ;
  [%expect
    {|
[JSON-0:]
   { Change.json =
     `O ([("sha", `String ("Mock MR 1"));
           ("changes",
            `A ([`O ([("old_path", `String ("some/old/path"));
                       ("new_path", `String ("some/new/path"));
                       ("deleted_file", `Bool (false));
                       ("diff", `String ("some diff"))]);
                  `O ([("old_path", `String ("docs"));
                        ("new_path", `String ("docs"));
                        ("deleted_file", `Bool (false));
                        ("diff", `String ("some diff"))]);
                  `O ([("old_path", `String ("src/lib_p2p/p2p"));
                        ("new_path", `String ("src/lib_p2p/p2p"));
                        ("deleted_file", `Bool (false));
                        ("diff", `String ("some diff"))])
                  ]))
           ]);
     sha = "Mock MR 1"; old_path = "some/old/path"; new_path = "some/new/path";
     deleted_file = false; diff = "some diff" }
   { Change.json =
     `O ([("sha", `String ("Mock MR 1"));
           ("changes",
            `A ([`O ([("old_path", `String ("some/old/path"));
                       ("new_path", `String ("some/new/path"));
                       ("deleted_file", `Bool (false));
                       ("diff", `String ("some diff"))]);
                  `O ([("old_path", `String ("docs"));
                        ("new_path", `String ("docs"));
                        ("deleted_file", `Bool (false));
                        ("diff", `String ("some diff"))]);
                  `O ([("old_path", `String ("src/lib_p2p/p2p"));
                        ("new_path", `String ("src/lib_p2p/p2p"));
                        ("deleted_file", `Bool (false));
                        ("diff", `String ("some diff"))])
                  ]))
           ]);
     sha = "Mock MR 1"; old_path = "docs"; new_path = "docs";
     deleted_file = false; diff = "some diff" }
   { Change.json =
     `O ([("sha", `String ("Mock MR 1"));
           ("changes",
            `A ([`O ([("old_path", `String ("some/old/path"));
                       ("new_path", `String ("some/new/path"));
                       ("deleted_file", `Bool (false));
                       ("diff", `String ("some diff"))]);
                  `O ([("old_path", `String ("docs"));
                        ("new_path", `String ("docs"));
                        ("deleted_file", `Bool (false));
                        ("diff", `String ("some diff"))]);
                  `O ([("old_path", `String ("src/lib_p2p/p2p"));
                        ("new_path", `String ("src/lib_p2p/p2p"));
                        ("deleted_file", `Bool (false));
                        ("diff", `String ("some diff"))])
                  ]))
           ]);
     sha = "Mock MR 1"; old_path = "src/lib_p2p/p2p";
     new_path = "src/lib_p2p/p2p"; deleted_file = false; diff = "some diff" }
[JSON-1:]
   { Change.json =
     `O ([("sha", `String ("Mock MR 2"));
           ("changes",
            `A ([`O ([("old_path", `String ("some/old/path"));
                       ("new_path", `String ("some/new/path"));
                       ("deleted_file", `Bool (false));
                       ("diff", `String ("some diff"))]);
                  `O ([("old_path",
                        `String ("src/lib_p2p_services/p2p_errors.ml"));
                        ("new_path",
                         `String ("src/lib_p2p_services/p2p_errors.ml"));
                        ("deleted_file", `Bool (false));
                        ("diff", `String ("some diff"))])
                  ]))
           ]);
     sha = "Mock MR 2"; old_path = "some/old/path"; new_path = "some/new/path";
     deleted_file = false; diff = "some diff" }
   { Change.json =
     `O ([("sha", `String ("Mock MR 2"));
           ("changes",
            `A ([`O ([("old_path", `String ("some/old/path"));
                       ("new_path", `String ("some/new/path"));
                       ("deleted_file", `Bool (false));
                       ("diff", `String ("some diff"))]);
                  `O ([("old_path",
                        `String ("src/lib_p2p_services/p2p_errors.ml"));
                        ("new_path",
                         `String ("src/lib_p2p_services/p2p_errors.ml"));
                        ("deleted_file", `Bool (false));
                        ("diff", `String ("some diff"))])
                  ]))
           ]);
     sha = "Mock MR 2"; old_path = "src/lib_p2p_services/p2p_errors.ml";
     new_path = "src/lib_p2p_services/p2p_errors.ml"; deleted_file = false;
     diff = "some diff" }
[JSON-2:]
   { Change.json =
     `O ([("sha", `String ("Mock MR 3"));
           ("changes",
            `A ([`O ([("old_path", `String ("some/old/path"));
                       ("new_path", `String ("some/new/path"));
                       ("deleted_file", `Bool (false));
                       ("diff", `String ("some diff"))]);
                  `O ([("old_path",
                        `String ("src/proto_013_PtJakart/lib_benchmark/autocomp.ml"));
                        ("new_path",
                         `String ("src/proto_013_PtJakart/lib_benchmark/autocomp.ml"));
                        ("deleted_file", `Bool (false));
                        ("diff", `String ("some diff"))]);
                  `O ([("old_path",
                        `String ("src/proto_014_PtKathma/lib_benchmark/execution_context.ml"));
                        ("new_path",
                         `String ("src/proto_014_PtKathma/lib_benchmark/execution_context.ml"));
                        ("deleted_file", `Bool (true));
                        ("diff", `String ("some diff"))])
                  ]))
           ]);
     sha = "Mock MR 3"; old_path = "some/old/path"; new_path = "some/new/path";
     deleted_file = false; diff = "some diff" }
   { Change.json =
     `O ([("sha", `String ("Mock MR 3"));
           ("changes",
            `A ([`O ([("old_path", `String ("some/old/path"));
                       ("new_path", `String ("some/new/path"));
                       ("deleted_file", `Bool (false));
                       ("diff", `String ("some diff"))]);
                  `O ([("old_path",
                        `String ("src/proto_013_PtJakart/lib_benchmark/autocomp.ml"));
                        ("new_path",
                         `String ("src/proto_013_PtJakart/lib_benchmark/autocomp.ml"));
                        ("deleted_file", `Bool (false));
                        ("diff", `String ("some diff"))]);
                  `O ([("old_path",
                        `String ("src/proto_014_PtKathma/lib_benchmark/execution_context.ml"));
                        ("new_path",
                         `String ("src/proto_014_PtKathma/lib_benchmark/execution_context.ml"));
                        ("deleted_file", `Bool (true));
                        ("diff", `String ("some diff"))])
                  ]))
           ]);
     sha = "Mock MR 3";
     old_path = "src/proto_013_PtJakart/lib_benchmark/autocomp.ml";
     new_path = "src/proto_013_PtJakart/lib_benchmark/autocomp.ml";
     deleted_file = false; diff = "some diff" }
   { Change.json =
     `O ([("sha", `String ("Mock MR 3"));
           ("changes",
            `A ([`O ([("old_path", `String ("some/old/path"));
                       ("new_path", `String ("some/new/path"));
                       ("deleted_file", `Bool (false));
                       ("diff", `String ("some diff"))]);
                  `O ([("old_path",
                        `String ("src/proto_013_PtJakart/lib_benchmark/autocomp.ml"));
                        ("new_path",
                         `String ("src/proto_013_PtJakart/lib_benchmark/autocomp.ml"));
                        ("deleted_file", `Bool (false));
                        ("diff", `String ("some diff"))]);
                  `O ([("old_path",
                        `String ("src/proto_014_PtKathma/lib_benchmark/execution_context.ml"));
                        ("new_path",
                         `String ("src/proto_014_PtKathma/lib_benchmark/execution_context.ml"));
                        ("deleted_file", `Bool (true));
                        ("diff", `String ("some diff"))])
                  ]))
           ]);
     sha = "Mock MR 3";
     old_path = "src/proto_014_PtKathma/lib_benchmark/execution_context.ml";
     new_path = "src/proto_014_PtKathma/lib_benchmark/execution_context.ml";
     deleted_file = true; diff = "some diff" }
[JSON-3:]
   { Change.json =
     `O ([("sha", `String ("Mock MR 4"));
           ("changes",
            `A ([`O ([("old_path", `String ("some/old/path"));
                       ("new_path", `String ("some/new/path"));
                       ("deleted_file", `Bool (false));
                       ("diff", `String ("some diff"))]);
                  `O ([("old_path", `String (".gitlab"));
                        ("new_path", `String (""));
                        ("deleted_file", `Bool (true));
                        ("diff", `String ("some diff"))])
                  ]))
           ]);
     sha = "Mock MR 4"; old_path = "some/old/path"; new_path = "some/new/path";
     deleted_file = false; diff = "some diff" }
   { Change.json =
     `O ([("sha", `String ("Mock MR 4"));
           ("changes",
            `A ([`O ([("old_path", `String ("some/old/path"));
                       ("new_path", `String ("some/new/path"));
                       ("deleted_file", `Bool (false));
                       ("diff", `String ("some diff"))]);
                  `O ([("old_path", `String (".gitlab"));
                        ("new_path", `String (""));
                        ("deleted_file", `Bool (true));
                        ("diff", `String ("some diff"))])
                  ]))
           ]);
     sha = "Mock MR 4"; old_path = ".gitlab"; new_path = "";
     deleted_file = true; diff = "some diff" } |}]
