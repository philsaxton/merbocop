(* Copyright © 2021 Merbocop Team <contact@tqtezos.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE. *)

open! Base
open Merbocop_lib
open Cmdliner
open Term

let say f = Fmt.kstr (Caml.Printf.eprintf "[merbocop:] %s\n%!") f
let help = Term.(ret (const (`Help (`Auto, None))), info "merbocop")

(** [access_token_term] is [`Access_token of string option].
 **  This transforms the command line option [--access-token] [TOKEN] into an OCaml value. *)
let access_token_term () =
  let token_is_valid s =
    (* Cf. https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token-programmatically *)
    String.length s = 20
    && String.for_all s ~f:(function ' ' | '\n' | '\t' -> false | _ -> true)
  in
  Arg.(
    const (function
      | None -> `Access_token None
      | Some s when token_is_valid s -> `Access_token (Some s)
      | Some other -> Fmt.failwith "Wrong format for access token: %S" other )
    $ value
        (opt (some string) None
           (info ["access-token"] ~doc:"Gitlab API access token.") ))

(** [self_id_term] is [`Self_id of int option].
 **  This transforms the command line option [--self-id] [USER-ID] into an OCaml value. *)
let self_id_term () =
  Arg.(
    const (fun s -> `Self_id s)
    $ value
        (opt (some int) None
           (info ["self-id"] ~doc:"Gitlab API user ID of the bot.") ))

(** [commits_to_inspect_term] is [`Commits_to_inspect of (int * string) list].
 **  This transforms the command line option [--inspect-commit] [PROJECT:COMMIT] into an OCaml value. *)
let commits_to_inspect_term () =
  Arg.(
    const (fun l -> `Commits_to_inspect l)
    $ value
        (opt_all (t2 ~sep:':' int string) []
           (info ["inspect-commit"] ~doc:"project:commit to inspect") ))

(** [whole_project_inspection_term] is [`Inspect_whole_projects of (stirng * int option) list].
 **  This transforms the command line option [--whole-project-inspection] [POJECT:LIMIT] into an OCaml value. *)
let whole_project_inspection_term () =
  Arg.(
    const (fun s -> `Inspect_whole_projects s)
    $ value
        (opt_all
           (pair ~sep:':' string (some int))
           []
           (info ["whole-project-inspection"] ~doc:"Gitlab API project ID.") ))

(** [limit_to_recent_updates_term] is [`Only_updated_since of (int * string) option].
 **  This transforms the command line option [--only-updated-in-the-past] [NUMBER:DAYS] into an OCaml value. *)
let limit_to_recent_updates_term () =
  term_result
    Arg.(
      const (fun since for_units ->
          match (for_units, since) with
          | None, None -> Ok (`Only_updated_since None)
          | None, (Some _ as s) -> Ok (`Only_updated_since s)
          | Some (days, `Days), None ->
              let span =
                let seconds = Float.(days * 60. * 60. * 24.) in
                Option.value_exn ~message:"ptime span"
                  (Ptime.Span.of_float_s seconds) in
              let now =
                Option.value_exn ~message:"ptime now"
                  (Ptime.of_float_s (Unix.gettimeofday ())) in
              let date =
                Option.value_exn ~message:"ptime sub" (Ptime.sub_span now span)
              in
              let as_string = Ptime.to_rfc3339 date in
              Ok (`Only_updated_since (Some as_string))
          | Some (_months, `Months), None -> Error (`Msg "TODO months")
          | Some _, Some _ ->
              Error
                (`Msg
                  "Cannot provide --only-updated-since and --only-in-the-past"
                  ) )
      $ value
          (opt (some string) None
             (info ["only-updated-since"] ~doc:"Restrict to MRs updated since."
                ~docv:"RAW-STING-DATE" ) )
      $ value
          (opt
             (some
                (pair ~sep:':' float
                   (enum [("days", `Days); ("months", `Months)]) ) )
             None
             (info ["only-updated-in-the-past"] ~doc:"Restrict MRs") ))

(** [label_mr] applies (or removes) a Gitlab, label of [doc_label] and/or [approved_label]
 **  for all merger request in [mr_list]. *)
let label_mr (self_id : int option) (token : string option)
    (retry_options : Web_api.Retry_options.t)
    (mr_list : (string * Merge_request.mr_result) list)
    (errors : string list ref) (approving : bool) (doc_label : string option)
    (approved_label : string option) =
  List.iter mr_list ~f:(function
    | _, Error (`Mr_not_found _) -> ()
    | project, Ok mr -> (
        let labels = Analysis.labels_of_mr mr in
        let add_remove_lists =
          let open Option in
          List.fold labels ~init:([], []) ~f:(fun (add, remove) label ->
              match label with
              | `Doc_only -> (add @ to_list doc_label, remove)
              | `Not_doc_only -> (add, remove @ to_list doc_label)
              | `Approvals_needed n -> (
                match n with
                | n when n < 0 -> (add @ to_list approved_label, remove)
                | 0 ->
                    if
                      approving
                      (* We check if Mebocop's un/approval will change `Approvals_needed n *)
                      && List.mem labels `Not_doc_only ~equal:Poly.equal
                      && List.exists mr.approvals.approved_by ~f:(fun user ->
                             mem self_id user ~equal:Int.equal )
                    then (add, remove @ to_list approved_label)
                    else (add @ to_list approved_label, remove)
                | 1 ->
                    if
                      approving
                      && List.mem labels `Doc_only ~equal:Poly.equal
                      && not
                           (List.exists mr.approvals.approved_by ~f:(fun user ->
                                mem self_id user ~equal:Int.equal ) )
                    then (add @ to_list approved_label, remove)
                    else (add, remove @ to_list approved_label)
                | _ -> (add, remove @ to_list approved_label) ) ) in
        match
          ( Bot_actions.label_mr ?token ~project ~mr ~retry_options
              (fst add_remove_lists)
          , Bot_actions.unlabel_mr ?token ~project ~mr ~retry_options
              (snd add_remove_lists) )
        with
        | Ok (Some (_ : Ezjsonm.value)), Ok (Some (_ : Ezjsonm.value))
         |Ok (Some (_ : Ezjsonm.value)), Ok None
         |Ok None, Ok (Some (_ : Ezjsonm.value))
         |Ok None, Ok None ->
            ()
        | Error e, _ | _, Error e ->
            errors :=
              !errors
              @ [ Fmt.str
                    "An error occured while labeling merge request #%d with \
                     the following exception:%a\n"
                    mr.id Exn.pp e ] ) )

(** [set_doc_label] is [`Set_doc_label of string option].
 **  This transforms the command line option [--set-doc-label] [LABEL] into an OCaml value. *)
let set_doc_label () =
  Arg.(
    const (fun s -> `Set_doc_label s)
    $ value
        (opt (some string) None
           (info ["set-doc-label"] ~docv:"LABEL"
              ~doc:"Un/set label for doc-only Merge Requests." ) ))

(** [set_approved_label] is [`Set_approved_label of string option].
 **  This transforms the command line option [--set-approved-label] [LABEL] into an OCaml value. *)
let set_approved_label () =
  Arg.(
    const (fun s -> `Set_approved_label s)
    $ value
        (opt (some string) None
           (info ["set-approved-label"] ~docv:"LABEL"
              ~doc:"Add/Remove label for approved Merge Requests." ) ))

(** [approve_mr] approves (or unapproves) each merger request in [mr_list] based on its
 **  [`Doc_only | `Not_doc_only] status. *)
let approve_mr (self_id : int option) (token : string option)
    (retry_options : Web_api.Retry_options.t)
    (mr_list : (string * Merge_request.mr_result) list)
    (errors : string list ref) (approve_doc_only_mr : bool) =
  List.iter mr_list ~f:(function
    | _, Error (`Mr_not_found _) -> ()
    | project, Ok mr ->
        let reporting_only = if approve_doc_only_mr then false else true in
        List.iter (Analysis.labels_of_mr mr) ~f:(function
          | `Doc_only -> (
            match
              Bot_actions.approve_mr ?user_id:self_id ?token ~reporting_only
                ~project ~retry_options mr
            with
            | Ok None | Ok (Some (_ : Ezjsonm.value)) -> ()
            | Error e ->
                errors :=
                  !errors
                  @ [ Fmt.str
                        "An error occured while approving merge request #%d \
                         with the following exception:%a\n"
                        mr.id Exn.pp e ] )
          | `Not_doc_only -> (
            match
              Bot_actions.unapprove_mr ?user_id:self_id ?token ~reporting_only
                ~project ~retry_options mr
            with
            | Ok None | Ok (Some (_ : Ezjsonm.value)) -> ()
            | Error e ->
                errors :=
                  !errors
                  @ [ Fmt.str
                        "An error occured while unapproving merge request #%d \
                         with the following exception:%a\n"
                        mr.id Exn.pp e ] )
          | _ -> () ) )

(** [approve_doc_only_mr] is [`Approve_doc_only_mr of bool].
 **  This transforms the command line option [--approve-doc-only] into an OCaml value. *)
let approve_doc_only_mr () =
  Arg.(
    const (fun s -> `Approve_doc_only_mr s)
    $ value
        (flag
           (info ["approve-doc-only"]
              ~doc:
                "Approves doc-only merge request while setting the doc_only \
                 label and removes approval when removing the label." ) ))

(** [allow_failures] is [`Allow_failures of bool].
 **  This transforms the command line option [--allow-failures] into an OCaml value. *)
let allow_failures () =
  Arg.(
    const (fun s -> `Allow_failures s)
    $ value (flag (info ["allow-failures"] ~doc:"Return 0 for some errors.")))

(** [post_comments] will post (or edit existing) comments (Gitlabe notes) for all merge requests in [mr_list] *)
let post_comments (self_id : int option) (token : string option)
    (mr_list : (string * Merge_request.mr_result) list)
    (retry_options : Web_api.Retry_options.t) (errors : string list ref)
    (comment_on_self : bool) (warnings : Warnings.t) (do_edit_existing : bool)
    (require_resolve_thread : bool) =
  if comment_on_self then
    List.iter mr_list ~f:(function
      | _, Error (`Mr_not_found _) -> (* Already counted *) ()
      | project, Ok mr -> (
          let body_of_prev _previous_body =
            try Comment_body.simple_message mr |> String.concat ~sep:"\n"
            with e ->
              Fmt.str "ERROR: %a (%d MRs)" Exn.pp e (List.length mr_list) in
          match
            Bot_actions.post_or_edit_mr_comment_or_thread
              ~token:
                (Option.value_exn
                   ~message:
                     "Please provide the --access-token option to post \
                      comments."
                   token )
              ~do_edit_existing ~require_resolve_thread ~mr_result:(Ok mr)
              ~user_id:
                (Option.value_exn
                   ~message:"Commenting requires the --self-id option." self_id )
              ~retry_options ~warnings body_of_prev
          with
          | Ok (`Added res) | Ok (`Edited res) -> (
              say "Ok, done %s/%a %s." project Merge_request.pp_quick (Ok mr)
                ( Ezjsonm.value_to_string ~minify:true res
                |> fun s -> String.prefix s 35 ) ;
              errors :=
                !errors
                @
                match res with
                | `O [("message", `String msg)] ->
                    [ Fmt.str "Add-or-edit-comment: %s/%a: %s" project
                        Merge_request.pp_quick (Ok mr) msg ]
                | _ -> [] )
          | Error (`Mr_not_found (proj, s, js, e)) ->
              say "Cannot comment, no MR there: %s/%s\n%s\n%a" proj s
                (Ezjsonm.value_to_string ~minify:false js)
                Exn.pp e ) )

(** [post_on_self] is [`Comment_on_self of bool].
 **  This transforms the command line option [--post-on-self] to an OCaml value. *)
let post_on_self () =
  Arg.(
    const (fun s -> `Comment_on_self s)
    $ value (flag (info ["post-on-self"] ~doc:"Post")))

(** [no_comment_editing] is [`Edit_existing of bool].
 **  This transforms the command line arguement [--no-comment-editing] to an OCaml value. *)
let no_comment_editing () =
  Arg.(
    const (function
      | true -> `Edit_existing false
      | false -> `Edit_existing true )
    $ value
        (flag
           (info ["no-comment-editing"]
              ~doc:"Always add the comment, do not edit an existing one." ) ))

(** [full_report_body] prints Merbocop's report on all merge requests in [mr_list] to [write_body]. *)
let full_report_body (mr_list : (string * Merge_request.mr_result) list)
    (errors : string list ref) (write_body : string option) =
  let body_of_prev _previous_body =
    try Comment_body.full_report () ~mr_list:(List.map ~f:snd mr_list)
    with e ->
      errors := !errors @ [Fmt.str "Making multi-mr body: %a" Exn.pp e] ;
      Fmt.str "ERROR: %a (%d MRs)" Exn.pp e (List.length mr_list) in
  let tmp_md =
    match write_body with
    | Some p -> p
    | None -> Caml.Filename.temp_file "body" ".md" in
  let o = Caml.open_out tmp_md in
  Caml.output_string o (body_of_prev None) ;
  Caml.close_out o ;
  say "Body: file://%s\n" tmp_md

(** [write_body] is [`Write_body of string option].
 **  This transforms the command line option [--write-body] into an OCaml value. *)
let write_body () =
  Arg.(
    const (fun s -> `Write_body s)
    $ value
        (opt (some string) None
           (info ["write-body"]
              ~doc:"write the body of the message to PATH (markdown)."
              ~docv:"PATH" ) ))

(** [load_cache] is [`Load_cache of string option].
 **  This transforms the command line option [--load-cache] [PATH] into an OCaml value. *)
let load_cache () =
  Arg.(
    const (fun s -> `Load_cache s)
    $ value
        (opt (some string) None
           (info ["load-cache"] ~doc:"Load contents of the cache from PATH") ))

(** [dump_cache] is [`Dump_cache of string option].
 **  This transforms the command line option [--dump-cache] [PATH] into an OCaml value. *)
let dump_cache () =
  Arg.(
    const (fun s -> `Dump_cache s)
    $ value
        (opt (some string) None
           (info ["dump-cache"] ~doc:"Dump contents of the cache in PATH") ))

(** [get_retry_option options no_retry] is retry_options : Web_api.Retry_options.t
 **  for faild HTTP calls. Values are taken from [options : (int * float * float) option]
 **  [options : None] is the default values; (retries = 4, wait = 0.1 , factor = 2.0).
 **  [no_retry : true] will set all values ot zero. *)
let get_retry_options (options : (int * float * float) option) (no_retry : bool)
    : Web_api.Retry_options.t =
  let open Web_api.Retry_options in
  if no_retry then none
  else
    match options with
    | None -> web_api
    | Some (retries, wait, factor) -> make retries wait factor

(** [retry_options] is [`Retry_options of (int * float * float) option].
 **  This transforms the command line options [--http-retry-options] [RETRY-LIMIT:WAIT:FACTOR]
 **  into an OCaml valuse *)
let retry_options () =
  Arg.(
    const (fun s -> `Retry_options s)
    $ value
        (opt
           (some (t3 ~sep:':' int float float))
           Web_api.Retry_options.(
             Some (web_api.retries, web_api.wait, web_api.factor))
           (info ["http-retry-options"]
              ~doc:
                "Options for retries on failed HTTP calls. Use colon separted \
                 values RETRY-LIMIT:DELAY:FACTOR. RETRY-LIMIT (integer) limits \
                 the number of retries (default 4). WAIT (float) sets the \
                 initial delay in seconds (default 0.1). FACTOR (float) is the \
                 factor by wich the wait will grow on subsiquent retreis \
                 (default 2.0)" ) ))

let no_retry () =
  Arg.(
    const (fun s -> `No_retry s)
    $ value (flag (info ["http-no-retry"] ~doc:"Dont retry failed HTTP calls.")))

let make_warnings (mr_list : (string * Merge_request.mr_result) list) =
  let open Warnings in
  let job_warnigns = make (Comment_body.pipeline_links ()) in
  List.iter mr_list ~f:(function
    | _, Error (`Mr_not_found _) -> ()
    | _, Ok mr -> add ~warnings:job_warnigns ~mr () ) ;
  job_warnigns

(** [post_warnings token post_warnings_to mr_list retry_options errors] Post warnings as issue comments
 ** in [post_warnings_to]. *)
let post_warnings post_warnings_to (token : string option)
    (retry_options : Web_api.Retry_options.t) (errors : string list ref)
    (warnings : Warnings.t) =
  match post_warnings_to with
  | None -> ()
  | Some (project, issue, warn_on) -> (
    match
      Warnings.post_issue_comment ?token ~project ~issue_id:issue ~warn_on
        ~retry_options warnings
    with
    | Ok (Some (_ : Ezjsonm.value)) | Ok None -> ()
    | Error e ->
        errors :=
          !errors
          @ [ Fmt.str
                "An error occured while posting warnings for with the \
                 following exception:%a\n"
                Exn.pp e ] )

let post_warnings_to () =
  Arg.(
    const (fun s -> `Post_warnings s)
    $ value
        (opt
           (some
              (t3 ~sep:':' int int
                 (enum [("non-empty", `Non_empty); ("always", `Always)]) ) )
           None
           (info ["post-warnings-to"]
              ~doc:
                "Post merge request warnings to issue at PROJECT:ISSUE:ALWAYS" ) ))

let wip_status () =
  let choices =
    [("any", `Any); ("ready-only", `Only_ready); ("wip-only", `Only_wip)] in
  let doc =
    Fmt.str "Select a “draft” status to filter on (%s)"
      (List.map ~f:fst choices |> String.concat ~sep:"|") in
  Arg.(
    const (fun w -> `Wip_status w)
    $ value (opt (enum choices) `Any (info ["wip-status-filter"] ~doc)))

let () =
  let multi_inspect_cmd =
    ( const
        (fun
          (`Access_token token)
          (`Self_id self)
          (`Commits_to_inspect to_inspect)
          (`Allow_failures allow_failures)
          (`Inspect_whole_projects whole_projects_to_inspect)
          (`Only_updated_since date_opt)
          (`Wip_status wip_status)
          (`Comment_on_self comment_on_self)
          (`Edit_existing do_edit_existing)
          (`Write_body write_body)
          (`Load_cache opt_load_path)
          (`Dump_cache opt_path)
          (`Set_doc_label doc_label)
          (`Set_approved_label approved_label)
          (`Approve_doc_only_mr approve_doc_only)
          (`Retry_options options)
          (`No_retry no_retry)
          (`Post_warnings post_warnings_to)
          (`Assign_reviewers assign_reviewers)
          (`Require_resolve_thread resolve_required)
        ->
          Option.iter opt_load_path ~f:(fun path -> Web_api.load_cache ~path) ;
          Caml.at_exit (fun () ->
              Option.iter opt_path ~f:(fun path -> Web_api.dump_cache ~path) ) ;
          let retry = get_retry_options options no_retry in
          let errors = ref [] in
          (let mr_list =
             List.map to_inspect ~f:(fun (project_id, ci) ->
                 let project = Int.to_string project_id in
                 ( project
                 , Merge_request.find_by_commit ?token ~project
                     ~retry_options:retry ci ) )
             @ List.concat_map whole_projects_to_inspect
                 ~f:(fun (project, limit) ->
                   Merge_request.all_from_project ?token ~retry_options:retry
                     ~wip_status project
                   |> (fun l ->
                        match date_opt with
                        | None -> l
                        | Some date ->
                            List.filter l ~f:(fun (_, mrres) ->
                                match Merge_request.most_recent_push mrres with
                                | None -> false
                                | Some d -> String.compare d date >= 0 ) )
                   |> fun l ->
                   match limit with Some lim -> List.take l lim | None -> l )
           in
           let job_warnings = make_warnings mr_list in
           errors :=
             !errors
             @ List.filter_map mr_list ~f:(function
                 | _, Ok _ -> None
                 | _, Error (`Mr_not_found (proj, ci, _, e)) ->
                     Some (Fmt.str "MR-not-found: %s/%s: %a" proj ci Exn.pp e) ) ;
           if comment_on_self then
             post_comments self token mr_list retry errors comment_on_self
               job_warnings do_edit_existing resolve_required ;
           label_mr self token retry mr_list errors approve_doc_only doc_label
             approved_label ;
           approve_mr self token retry mr_list errors approve_doc_only ;
           Reviewers.exe token retry mr_list errors assign_reviewers ;
           full_report_body mr_list errors write_body ;
           post_warnings post_warnings_to token retry errors job_warnings ) ;
          match !errors with
          | [] -> Stdlib.exit 0
          | more ->
              say "There were errors:" ;
              List.iter more ~f:(say "- %s") ;
              Stdlib.exit (if allow_failures then 0 else 3) )
      $ access_token_term () $ self_id_term () $ commits_to_inspect_term ()
      $ allow_failures ()
      $ whole_project_inspection_term ()
      $ limit_to_recent_updates_term ()
      $ wip_status ()
      (* $ mr_to_comment_on_term () *)
      $ post_on_self ()
      $ no_comment_editing () $ write_body () $ load_cache () $ dump_cache ()
      $ set_doc_label () $ set_approved_label () $ approve_doc_only_mr ()
      $ retry_options () $ no_retry () $ post_warnings_to ()
      $ Reviewers.cli_term () $ Thread.cli_term ()
    , info "full-inspection" ~doc:"Do full inspection on a bunch of MRs" ) in
  exit (eval_choice (help : unit Term.t * _) [multi_inspect_cmd])
