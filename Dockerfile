FROM ocaml/opam:alpine-ocaml-4.12
RUN sudo apk update
RUN sudo apk add openssl-dev curl-dev
RUN sudo mkdir -p /rebuild
RUN sudo chown -R opam:opam /rebuild
WORKDIR /rebuild
ADD --chown=opam:opam . ./
RUN ./please.sh get_deps
RUN ./please.sh build
FROM alpine
RUN apk update
RUN apk add libcurl
COPY --from=0 /rebuild/_build/default/src/app/merbocop.exe /usr/bin/merbocop
